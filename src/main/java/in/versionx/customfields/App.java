package in.versionx.customfields;

import android.content.Context;

import androidx.room.Room;

import in.versionx.customfields.Database.AppDatabase;

public class App {

    static AppDatabase fieldsdb;
    private static Context context;
    private static final App ourInstance = new App();


    public static Context getAppContext() {
        return App.context;
    }

    public static App getInstance(Context mcontext) {

        context = mcontext.getApplicationContext();

        return ourInstance;
    }

    public static AppDatabase getDbInstance(Context mcontext) {
        context = mcontext.getApplicationContext();
        if (fieldsdb == null) {
            fieldsdb = Room.databaseBuilder(context,
                    AppDatabase.class, "customFieldsDB").build();
        }

        return fieldsdb;
    }


}
