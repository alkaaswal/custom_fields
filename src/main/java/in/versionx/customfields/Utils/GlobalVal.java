package in.versionx.customfields.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

public class GlobalVal {

    public static final String TEXTAREA = "ta";
    public static final String TEXTBOX = "tb";
    public static final String DROPDOWN = "dd";
    public static final String CHECKBOX = "cb";
    public static final String IMG = "img";
    public static final String SEARCHABLE = "srch";
    public static final String NUMBER = "num";
    public static final String DECIMAL_NUMBER = "dcml_num";
    public static final String SEPERATION = "sep";
    public static final String RADIOBUTTON = "rb";
    public static final String VISITOR_ID = "idProof";
    public static final String VEHICLE = "veh";
    public static final String COUNT_FIELD_ID = "count";
    public static final String BOXES = "box";
    public static final String DATE = "dt";
    public static final String SCREEN_COUNT = "count";
    public static final String CUSTOMFIELD_SP = "fieldSp";
    public static final String STAFF = "ST";
    public static final String RESIDENT = "RES";
    public static final String PARENT = "PA";
    public static final String PURPOSE = "p";
    public static final String FRONT_CAM ="cam";

    public static String SERVER_ERROR_MSG = "Server error! please try after some time";
    public static final String FIELD_DEPENDENCY_URL = "https://us-central1-dev-entry.cloudfunctions.net/getFieldDependency";
   // public static final String FIELD_DEPENDENCY_URL = "https://us-central1-whyentry.cloudfunctions.net/getFieldDependency";

    public static String NO_INTERNET_MSG = "No Internet Connection! Try again";
    public static String CHECK_INTERNET_MSG = "Check your internet connection and try again!";

    public static String getStringFromInputStream(InputStream stream) throws IOException {
        int n = 0;
        char[] buffer = new char[1024 * 4];
        InputStreamReader reader = new InputStreamReader(stream, "UTF8");
        StringWriter writer = new StringWriter();
        while (-1 != (n = reader.read(buffer))) writer.write(buffer, 0, n);
        return writer.toString();
    }

    public static Float getTextSize(String size) {
        switch (size) {
            case "":
            case "N":
                return 20f;
            case "M":
                return 22f;
            case "L":
                return 25f;
        }

        return 16f;
    }
}
