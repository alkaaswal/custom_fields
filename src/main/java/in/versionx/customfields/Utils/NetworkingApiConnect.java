package in.versionx.customfields.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;

import in.versionx.customfields.Database.FieldsAsyncTask;
import in.versionx.customfields.Interface.AsynctaskFinishedListner;
import in.versionx.customfields.Model.FieldsOptDep;


public class NetworkingApiConnect extends AsyncTask<Void, Void, String> {

    String apiUrl;
    String requestType;
    String data;
    URL url;
    AsynctaskFinishedListner listner;
    Context context;
    String flag;
    int timeout;
    int responseCode = 0;


    public NetworkingApiConnect(Context context, String apiUrl, String requestType, String data) {

        this.apiUrl = apiUrl;
        this.requestType = requestType;
        this.data = data;
        this.context = context;
        //  this.progressMessage = progressMessage;
      /*  this.flag = flag;     //
        this.timeout = timeout;*/
        /*if (!progressMessage.isEmpty())
            activity = (Activity) context;*/

    }

    public void onAsynctaskFinished(AsynctaskFinishedListner listner) {
        this.listner = listner;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       /* if (!progressMessage.isEmpty())
            progressdialog = ProgressDialog.show(context, progressMessage, "In Progress...");
*/
    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub

        HttpURLConnection conn = null;
        String response = null;

        try {
            url = new URL(apiUrl);

            conn = (HttpURLConnection) url.openConnection();

            if (timeout != 0)
                conn.setConnectTimeout(timeout);
        } catch (UnknownHostException e) {

        } catch (java.net.SocketTimeoutException e) {

        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        switch (requestType) {

            // this is for handling get requests

            case "get":
                try {
                    conn.setRequestMethod("GET");

                    responseCode = conn.getResponseCode();
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response = GlobalVal.getStringFromInputStream(in);

                } catch (UnknownHostException e) {

                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (java.net.SocketTimeoutException e) {

                } catch (FileNotFoundException e) {

                } catch (IOException e) {

                }
                break;

            // this is for handling post request
            case "post":

                try {
                    conn.setRequestMethod("POST");

                } catch (ProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                conn.setRequestProperty("Content-Type", "application/json");

                // conn.setRequestProperty("Accept", "application/json");
                try {
                    conn.connect();

                } catch (UnknownHostException e) {


                } catch (java.net.SocketTimeoutException e) {

                } catch (FileNotFoundException e) {

                } catch (IOException e) {

                }

                OutputStream os;
                try {
                    os = conn.getOutputStream();
                    OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                    osw.write(data);
                    osw.flush();
                    osw.close();
                    os.close();
                    responseCode = conn.getResponseCode();
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response = GlobalVal.getStringFromInputStream(in);

                } catch (UnknownHostException e) {

                } catch (java.net.SocketTimeoutException e) {

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            default:
                break;
        }

        // read the response


        return response;
    }


    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        // super.onPostExecute(result);

        try {
            if(result!=null) {
                JSONArray jsonArray = new JSONArray(result);


                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                    System.out.println("jsonObject is " + jsonObject);

                    FieldsOptDep fieldsOptDep = new FieldsOptDep();
                    fieldsOptDep.setId(jsonObject.getInt("id"));
                    fieldsOptDep.setpId(jsonObject.getInt("pId"));
                    fieldsOptDep.setfId(jsonObject.getString("fId"));
                    fieldsOptDep.setOptId(jsonObject.getString("optId"));
                    fieldsOptDep.setOptNm(jsonObject.getString("optNm"));
                    fieldsOptDep.setMod(jsonObject.getString("module"));
                    new FieldsAsyncTask(context, fieldsOptDep, FieldsOptDep.class.getName()).execute();


                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


      /*  if (result != null)
        listner.asynctaskFinished(result, responseCode, flag);*/

    }


}
