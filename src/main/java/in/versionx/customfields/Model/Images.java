package in.versionx.customfields.Model;

import java.io.Serializable;

/**
 * Created by Developer on 06/30/2017.
 */

public class Images {

    public String filePath, fieldId;

    public String dt;

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getDt() {
        return dt;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }


    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldId() {
        return fieldId;
    }
}
