package in.versionx.customfields.Model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Screen {
    @NonNull
    @PrimaryKey
    int id;

    @ColumnInfo(name = "title")
    String title;

    @ColumnInfo(name = "formId")
    String formId;

    @ColumnInfo(name = "order")
    int order;


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }


    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormId() {
        return formId;
    }


    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
