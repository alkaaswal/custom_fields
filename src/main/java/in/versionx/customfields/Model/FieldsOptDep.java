package in.versionx.customfields.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

@Entity
public class FieldsOptDep {

    @PrimaryKey
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "pId")
    public int pId;


    @ColumnInfo(name = "fId")
    public String fId;


    @ColumnInfo(name = "optId")
    public String optId;


    @ColumnInfo(name = "optNm")
    public String optNm;


    @ColumnInfo(name = "mod")
    public String mod;

    @Ignore
    public boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }


    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Ignore
    public ArrayList<String> allDepFields;

    public void setAllDepFields(ArrayList<String> allDepFields) {
        this.allDepFields = allDepFields;
    }

    public ArrayList<String> getAllDepFields() {
        return allDepFields;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getfId() {
        return fId;
    }

    public void setOptId(String optId) {
        this.optId = optId;
    }

    public String getOptId() {
        return optId;
    }

    public void setOptNm(String optNm) {
        this.optNm = optNm;
    }

    public String getOptNm() {
        return optNm;
    }

    public void setMod(String mod) {
        this.mod = mod;
    }

    public String getMod() {
        return mod;
    }
}
