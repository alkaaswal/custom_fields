package in.versionx.customfields.Model;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class FieldsDependency {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    int id;

    @ColumnInfo(name = "_id")
    String pId;

    @ColumnInfo(name = "fId")
    String fId;

    @ColumnInfo(name = "fval")
    String fval;




    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getfId() {
        return fId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getpId() {
        return pId;
    }

    public void setFval(String fval) {
        this.fval = fval;
    }

    public String getFval() {
        return fval;
    }
}
