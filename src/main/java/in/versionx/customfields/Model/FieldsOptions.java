package in.versionx.customfields.Model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class FieldsOptions {

    @ColumnInfo(name = "fId")
    public String fId;


    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "optionId")
    public String optionId;

    @ColumnInfo(name = "name")
    public String name;


    @ColumnInfo(name = "typ")
    public String typ;

    @Ignore
    public boolean isSelected;

    @Ignore
    public int pId;

    public void setpId(int pId) {
        this.pId = pId;
    }

    public int getpId() {
        return pId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getTyp() {
        return typ;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionId() {
        return optionId;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof String) {
            String name = obj.toString();
            return name.equals(this.name);
        }

        return super.equals(obj);

    }
}
