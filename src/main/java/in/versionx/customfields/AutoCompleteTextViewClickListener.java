package in.versionx.customfields;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

public class AutoCompleteTextViewClickListener implements AdapterView.OnItemClickListener {
    AutoCompleteTextView mAutoComplete;
    AdapterView.OnItemClickListener mOriginalListener;

    public AutoCompleteTextViewClickListener(AutoCompleteTextView acTextView,
                                             AdapterView.OnItemClickListener originalListener) {
        mAutoComplete = acTextView;
        mOriginalListener = originalListener;
    }

    public void onItemClick(AdapterView<?> adView, View view, int position,
                            long id) {
        mOriginalListener.onItemClick(adView, mAutoComplete, position, id);
    }
}