package in.versionx.customfields.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptDep;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.Model.Screen;


@Database(entities = {Fields.class, FieldsOptions.class, FieldsOptDep.class, Screen.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    public abstract FieldsDao fieldDao();


    public abstract FieldOptionsDao fieldOptionsDao();

    public abstract ScreenDao screenDao();


    public abstract FieldsOptDepDao fieldsDepDao();


}