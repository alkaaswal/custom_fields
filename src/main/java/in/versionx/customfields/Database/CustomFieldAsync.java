package in.versionx.customfields.Database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.App;
import in.versionx.customfields.Interface.FieldsAsyncTaskListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;


public class CustomFieldAsync extends AsyncTask<Void, Void, List<Fields>> {

    //Prevent leak
    private Context context;
    private Fields customFields;
    private FieldsOptions fieldsOptions;
    //  FieldMapping fieldMapping;
    String table;
    String action;
    FieldsAsyncTaskListner listner;
    String mod, formId;
    int scrn;
    //SharedPreferences spLogin;


    public CustomFieldAsync(Context context, String action, String mod, String formId) {
        this.context = context;
        this.action = action;
        this.mod = mod;
        this.formId = formId;
        // spLogin = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

    }

    public CustomFieldAsync(Context context, String action, String mod, int scrn) {
        this.context = context;
        this.action = action;
        this.mod = mod;
        this.scrn = scrn;
        // spLogin = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);

    }

    public void setListner(FieldsAsyncTaskListner listner) {
        this.listner = listner;
    }


    @Override
    protected List<Fields> doInBackground(Void... params) {
          /*  AgentDao agentDao = MyApp.DatabaseSetup.getDatabase().agentDao();
            return agentDao.agentsCount(email, phone, license);*/

        switch (action) {
            case "GET": {


                List<Fields> customFieldsList = App.getDbInstance(context).fieldDao().getAll(mod, formId);
                List<Fields> fieldsList = new ArrayList<>();
                fieldsList.addAll(customFieldsList);

                /* if (spLogin.getString(Global.FORM_RESTRICT_ID, "0").equals("0")) {
                 *//*List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().
                            getAll(spLogin.getString(Global.FORM_RESTRICT_ID, "rect1"));*//*
                    List<RestrictFields> restrictFieldsList = App.getDbInstance().restrictFieldDao().
                            getAll("rest1");

                    for (RestrictFields restrictFields : restrictFieldsList) {
                        for (CustomFields customFields : customFieldsList) {
                            if (restrictFields.getfId().equalsIgnoreCase(customFields.getId())) {

                                int j = fieldsList.indexOf(customFields);

                                if (restrictFields.isDel()) {
                                    fieldsList.remove(j);
                                } else {
                                        customFields.setHide(restrictFields.isHide());
                                    customFields.setRequired(restrictFields.isReq());
                                    fieldsList.set(j, customFields);
                                }
                            }
                        }
                    }
                    customFieldsList.clear();
                    customFieldsList.addAll(fieldsList);
                }*/
                return customFieldsList;
            }

            case "GETBYSCREEN": {

                List<Fields> customFieldsList = App.getDbInstance(context).fieldDao().getFieldsByScreen(mod, scrn);
                List<Fields> fieldsList = new ArrayList<>();
                fieldsList.addAll(customFieldsList);


                return customFieldsList;
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(List result) {
        super.onPostExecute(result);

        if (result != null) {
            listner.OnFieldsLoaded(result);

        }
    }


}
