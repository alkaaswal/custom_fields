package in.versionx.customfields.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.versionx.customfields.Model.FieldsOptDep;

@Dao
public interface FieldsOptDepDao {


    @Query("SELECT * FROM FieldsOptDep")
    List<FieldsOptDep> getAll();

    @Query("SELECT * FROM FieldsOptDep WHERE `fId` = :fId AND `optId` =:optId AND `pId`=:pId" )
    List<FieldsOptDep> getId(String fId, String optId,int pId);

    @Query("SELECT * FROM FieldsOptDep WHERE `id` = :id" )
    List<FieldsOptDep> checkIfExits(int id);

    @Query("SELECT * FROM FieldsOptDep WHERE `pId` = :pId" )
    List<FieldsOptDep> getDepFieldOpt(int pId);




    @Query("SELECT * FROM FieldsOptDep WHERE `fId` = :fId AND `optId`= :optId")
    List<FieldsOptDep> getDatabyFidOpt(String fId,String optId);

    @Query("SELECT * FROM FieldsOptDep WHERE `fId` = :fId")
    List<FieldsOptDep> getDatabyFid(String fId);


    @Query("DELETE  FROM FieldsOptDep WHERE `id` = :fId")
    void delete(int fId);

    @Query("DELETE  FROM FieldsOptDep")
    void deleteAll();

    @Insert
    void insertAll(FieldsOptDep... fields);


    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(FieldsOptDep... fields);
}
