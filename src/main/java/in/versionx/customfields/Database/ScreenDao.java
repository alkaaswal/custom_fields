package in.versionx.customfields.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.Screen;

@Dao
public interface ScreenDao {


  /*  @Query("SELECT * FROM Fields Where `mod` =:mod ORDER BY `order` ASC")
    List<Fields> getAll(String mod);*/

    @Query("SELECT * FROM Screen WHERE `id` = :id AND `formId`=:formId")
    List<Screen> getFieldsByScreenId(String formId,int id);

    @Query("DELETE  FROM Screen")
    void delete();


    @Insert
    void insertAll(Screen... screens);


    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Screen... screens);

}
