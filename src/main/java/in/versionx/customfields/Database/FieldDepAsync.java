package in.versionx.customfields.Database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.App;
import in.versionx.customfields.Interface.FieldsAsyncTaskListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptDep;
import in.versionx.customfields.Model.FieldsOptions;

public class FieldDepAsync extends AsyncTask<Void, Void, List<FieldsOptDep>> {
    Context context;
    String fId, optId, action;
    FieldsAsyncTaskListner listner;
    ArrayList<String> depFieldIds = null;
    private List<FieldsOptDep> fieldsOpts;
    Fields fields;
    int pId;


    public FieldDepAsync(Context context, FieldsOptions options, String action, Fields fields) {
        this.context = context;
        this.fId = options.fId;
        this.optId = options.optionId;
        this.action = action;
        this.fields = fields;
        this.pId = options.pId;

    }

    public FieldDepAsync(Context context, String fId, String action) {
        this.context = context;
        this.action = action;
        this.fId = fId;

    }

    @Override
    protected List<FieldsOptDep> doInBackground(Void... voids) {
        List<FieldsOptDep> fieldsOptDeps = null;

        switch (action) {

            case "GETDEPOPT":


                List<FieldsOptDep> field = App.getDbInstance(context).fieldsDepDao().getId(fId, optId, pId);

                if (field.size() > 0) {

                    fieldsOptDeps = App.getDbInstance(context).fieldsDepDao().getDepFieldOpt(field.get(0).getId());


                    getFieldsIds(field);
                }


                break;

            case "CLEARFIELD":

                List<FieldsOptDep> depFieldOpt = App.getDbInstance(context).fieldsDepDao().getDatabyFid(fId);

                if (depFieldOpt.size() > 0) {


                    getFieldsIds(depFieldOpt);
                }

        }


        return fieldsOptDeps;
    }


    private void getFieldsIds(List<FieldsOptDep> depFieldsId) {


        for (FieldsOptDep fieldsOptDep : depFieldsId) {


            List<FieldsOptDep> fieldsIds = App.getDbInstance(context).fieldsDepDao().getDepFieldOpt(fieldsOptDep.getId());

            if (depFieldIds == null) {
                depFieldIds = new ArrayList<>();
            }

            for (FieldsOptDep fields : fieldsIds) {
                if (!depFieldIds.contains(fields.getfId())) {
                    depFieldIds.add(fields.getfId());

                    List<FieldsOptDep> subFieldsOpt = App.getDbInstance(context).fieldsDepDao().getDepFieldOpt(fields.getId());
                    getFieldsIds(subFieldsOpt);
                }

            }


        }


       /* if (fieldsOpts != null && fieldsOpts.size() > 0)
            getFieldsIds(fieldsOpts);*/


    }


    @Override
    protected void onPostExecute(List<FieldsOptDep> fieldsOptDeps) {
        super.onPostExecute(fieldsOptDeps);
        if (action.equals("GETDEPOPT"))
            listner.onDepOptLoaded(fieldsOptDeps, depFieldIds, fields);
        else
            listner.onDepClear(depFieldIds);

    }

    public void setListner(FieldsAsyncTaskListner listner) {
        this.listner = listner;
    }
}
