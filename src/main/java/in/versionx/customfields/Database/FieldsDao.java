package in.versionx.customfields.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.versionx.customfields.Model.Fields;

@Dao
public interface FieldsDao {


    @Query("SELECT * FROM Fields Where `mod` =:mod AND `formId` =:formId ORDER BY `order` ASC")
    List<Fields> getAll(String mod, String formId);

    @Query("SELECT * FROM Fields WHERE `id` = :fId")
    List<Fields> getFieldById(String fId);

    @Query("DELETE  FROM Fields WHERE `id` = :fId")
    void delete(String fId);


    @Query("SELECT * FROM Fields Where `mod` =:mod AND `scrn` = :scrn ORDER BY `order` ASC")
    List<Fields> getFieldsByScreen(String mod, int scrn);


    @Insert
    void insertAll(Fields... fields);


    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Fields... fields);

}
