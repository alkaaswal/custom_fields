package in.versionx.customfields.Database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.App;
import in.versionx.customfields.Interface.FieldsAsyncTaskListner;
import in.versionx.customfields.Model.FieldsOptions;


public class FieldOptionsAsync extends AsyncTask<Void, Void, List<FieldsOptions>> {

    //Prevent leak
    private Context context;

    String table;
    String action, fId;
    FieldsAsyncTaskListner listner;
    String[] depOptIds, restrictOpts;
    //  SharedPreferences loginSp;

    static AppDatabase db;



    public FieldOptionsAsync(Context context, String action, String depOptIds, String fId) {
        this.context = context;
        this.action = action;
        if (depOptIds != null && !depOptIds.isEmpty())
            this.depOptIds = depOptIds.trim().split(",");
        //  loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        this.fId = fId;



    }


    public void setListner(FieldsAsyncTaskListner listner) {
        this.listner = listner;
    }


    @Override
    protected List<FieldsOptions> doInBackground(Void... params) {

        List<FieldsOptions> options = null;
        switch (action) {
            case "GETALL": {
                options = App.getDbInstance(context).fieldOptionsDao().getAll();
                ArrayList<FieldsOptions> fieldsOptions = new ArrayList<>();
                fieldsOptions.addAll(options);

                return options;
            }
            case "GETDEP": {
                if (depOptIds != null) {
                    options = App.getDbInstance(context).fieldOptionsDao().getDependencyOptions(depOptIds);
                }
                return options;
            }


        }


        return null;
    }

    @Override
    protected void onPostExecute(List result) {
        super.onPostExecute(result);

        if (result != null) {
            if (action.equals("GETALL")) {
                listner.OnOptionsLoaded(result);
            }
        }
    }


}
