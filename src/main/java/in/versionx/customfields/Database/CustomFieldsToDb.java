package in.versionx.customfields.Database;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Model.Screen;
import in.versionx.customfields.Utils.NetworkingApiConnect;


public class CustomFieldsToDb {
    Context context;
    String mod, formId = "";


    public CustomFieldsToDb(Context context, String mod) {
        this.context = context;
        this.mod = mod;


    }

    public CustomFieldsToDb(Context context, String mod, String formId) {
        this.context = context;
        this.mod = mod;
        this.formId = formId;


    }


    public void saveCustomFieldsToDB(DatabaseReference dbUrl) {


        dbUrl.orderByChild("o").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                setFields(dataSnapshot, "INSERT", false, null);

                if (dataSnapshot.hasChild("f")) {


                    for (DataSnapshot subFields : dataSnapshot.child("f").getChildren()) {


                        setFields(subFields, "INSERT", true, dataSnapshot.getKey());


                    }
                }


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                setFields(dataSnapshot, "UPDATE", false, null);

                if (dataSnapshot.hasChild("f")) {


                    for (DataSnapshot subFields : dataSnapshot.child("f").getChildren()) {


                        setFields(subFields, "UPDATE", true, dataSnapshot.getKey());


                    }
                }


            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {


                setFields(dataSnapshot, "DEL", false, null);

                if (dataSnapshot.hasChild("f")) {


                    for (DataSnapshot subFields : dataSnapshot.child("f").getChildren()) {


                        setFields(subFields, "DEL", true, dataSnapshot.getKey());


                    }
                }


            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    private void setFields(DataSnapshot dataSnapshot, String action, boolean isSub, String pFid) {
        Fields customFields = new Fields();

        if (dataSnapshot.child("nm").hasChildren()) {

            String multiNm = "";
            for (DataSnapshot nmSnap : dataSnapshot.child("nm").getChildren()) {

                if (!multiNm.isEmpty()) {
                    multiNm = multiNm + " / " + nmSnap.getValue(String.class);
                } else {
                    multiNm = nmSnap.getValue(String.class);
                }
            }

            customFields.setName(multiNm.trim());


        } else {
            if (dataSnapshot.hasChild("nm"))
                customFields.setName(dataSnapshot.child("nm").getValue(String.class));
            else
                customFields.setName("");
        }

        customFields.setMod(mod);

        if (isSub)
            customFields.setpFid(pFid);
        customFields.setId(dataSnapshot.getKey());
        if (dataSnapshot.hasChild("hide"))
            customFields.setHide(dataSnapshot.child("hide").getValue(Boolean.class));
        else
            customFields.setHide(false);

        if (dataSnapshot.child("re").exists())
            customFields.setReenter(dataSnapshot.child("re").getValue(Boolean.class));
        else
            customFields.setReenter(false);

        if (dataSnapshot.child("req").exists()) {
            customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));
            if (!customFields.getName().isEmpty() && dataSnapshot.child("req").getValue(Boolean.class))
                customFields.setName(customFields.getName() + " *");
            else
                customFields.setName(customFields.getName());
        } else {
            customFields.setName(customFields.getName());
            customFields.setRequired(false);
        }


        if (dataSnapshot.child("mul").exists()) {
            customFields.setMulti(dataSnapshot.child("mul").getValue(Boolean.class));
        } else
            customFields.setMulti(false);

        if (dataSnapshot.hasChild("align")) {
            customFields.setAlign(dataSnapshot.child("align").getValue(String.class));
        } else {
            customFields.setAlign("L");
        }

        if (dataSnapshot.hasChild("scrn")) {
            customFields.setScreen(dataSnapshot.child("scrn").getValue(Integer.class));
        } else {
            customFields.setScreen(1);
        }

        if (!formId.isEmpty() && dataSnapshot.hasChild("fId") && dataSnapshot.child("fId").hasChild(formId))
        {
            customFields.setFormId(formId);
        }
        else
            customFields.setFormId(dataSnapshot.getKey());




        customFields.setOrder(dataSnapshot.child("o").getValue(Integer.class));

        if (dataSnapshot.hasChild("dcml") && dataSnapshot.child("dcml").getValue(Boolean.class)) {
            customFields.setType("dcml_" + dataSnapshot.child("typ").getValue(String.class));
        } else {
            customFields.setType(dataSnapshot.child("typ").getValue(String.class));
        }


        if (dataSnapshot.hasChild("val"))
            customFields.setVal(dataSnapshot.child("val").getValue(String.class));
        else
            customFields.setVal("");

        if (dataSnapshot.hasChild("_h"))
            customFields.setFieldGrp(customFields.GRP_TOMEET);
        else
            customFields.setFieldGrp(customFields.GRP_WHOCAME);
        if (dataSnapshot.hasChild("req"))
            customFields.setRequired(dataSnapshot.child("req").getValue(Boolean.class));

        if (dataSnapshot.hasChild("srch"))
            customFields.setSrch(dataSnapshot.child("srch").getValue(Boolean.class));
        else
            customFields.setSrch(false);
        if (dataSnapshot.hasChild("edt"))
            customFields.setEdt(dataSnapshot.child("edt").getValue(Boolean.class));
        else
            customFields.setEdt(false);

        if (dataSnapshot.hasChild("lbl"))
            customFields.setLbl(dataSnapshot.child("lbl").getValue(String.class));

        if (dataSnapshot.hasChild("dsbl"))
            customFields.setDsbl(dataSnapshot.child("dsbl").getValue(Boolean.class));
        else
            customFields.setDsbl(true);
        if (dataSnapshot.hasChild("depRef")) {
            String depIds = "";
            for (DataSnapshot depSnap : dataSnapshot.child("depRef").getChildren()) {
                if (depIds.isEmpty())
                    depIds = depSnap.getKey();
                else
                    depIds = depIds + "," + depSnap.getKey();
            }
            customFields.setDepFids(depIds);
        }

        if (dataSnapshot.hasChild("dep")) {
            customFields.setDep(dataSnapshot.child("dep").getValue(boolean.class));
        } else {
            customFields.setDep(false);
        }


        if (dataSnapshot.hasChild("mod"))
            customFields.setModLbl(dataSnapshot.child("mod").getValue(String.class));

        if (dataSnapshot.child("typ").getValue(String.class).equals(GlobalVal.DROPDOWN)
                || dataSnapshot.child("typ").getValue(String.class).equals(GlobalVal.RADIOBUTTON)
                || dataSnapshot.child("typ").getValue(String.class).equals(GlobalVal.BOXES)
                || dataSnapshot.child("typ").getValue(String.class).equals(GlobalVal.CHECKBOX)) {

            for (DataSnapshot list : dataSnapshot.child("opt").getChildren()) {

                try {
                    FieldsOptions fieldsOptions = new FieldsOptions();
                    fieldsOptions.setfId(dataSnapshot.getKey());
                    fieldsOptions.setName(list.child("val").getValue().toString());
                    fieldsOptions.setOptionId(list.getKey());
                    fieldsOptions.setTyp(dataSnapshot.child("typ").getValue(String.class));
                    new FieldsAsyncTask(context, fieldsOptions, FieldsOptions.class.getName(), "INSERT").execute();


                } catch (Exception e) {

                }


            }
        }


        new FieldsAsyncTask(context, customFields, Fields.class.getName(), action).execute();


    }

    public void setDep(Context context, String bid, String gId, String mod) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bId", bid);
            jsonObject.put("gId", gId);
            jsonObject.put("mod", mod);

            new NetworkingApiConnect(context, GlobalVal.FIELD_DEPENDENCY_URL, "post", jsonObject.toString()).execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    /*    FieldsOptDep fieldsOptDep = new FieldsOptDep();
        fieldsOptDep.setfId("-LcpI8UEuMN1ByoRGPzN");
        fieldsOptDep.setpId(0);
        fieldsOptDep.setId(1);
        fieldsOptDep.setOptId("-LcpI8Z2DAhqyz91m46g");
        fieldsOptDep.setOptNm("VX");

        new FieldsAsyncTask(context, fieldsOptDep, FieldsOptDep.class.getName()).execute();
        FieldsOptDep fieldsOptDep2 = new FieldsOptDep();
        fieldsOptDep2.setfId("-LcpILRiUI_ZschrgjLJ");
        fieldsOptDep2.setpId(1);
        fieldsOptDep2.setId(2);
        fieldsOptDep2.setOptId("-LcpILWXb0njLdEbAzND");
        fieldsOptDep2.setOptNm("T1");
        new FieldsAsyncTask(context, fieldsOptDep2, FieldsOptDep.class.getName()).execute();

        FieldsOptDep fieldsOptDep3 = new FieldsOptDep();
        fieldsOptDep3.setfId("-LcpIT7bzVVg9CRvXx7F");
        fieldsOptDep3.setpId(2);
        fieldsOptDep3.setId(3);
        fieldsOptDep3.setOptId("-LcpITCOszKZfI42x5xX");
        fieldsOptDep3.setOptNm("F1");
        new FieldsAsyncTask(context, fieldsOptDep3, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep4 = new FieldsOptDep();
        fieldsOptDep4.setfId("-LcpILRiUI_ZschrgjLJ");
        fieldsOptDep4.setpId(1);
        fieldsOptDep4.setId(4);
        fieldsOptDep4.setOptId("-LcpILWYl8sCpD8KXfRH");
        fieldsOptDep4.setOptNm("T2");
        new FieldsAsyncTask(context, fieldsOptDep4, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep5 = new FieldsOptDep();
        fieldsOptDep5.setfId("-LcpIT7bzVVg9CRvXx7F");
        fieldsOptDep5.setpId(4);
        fieldsOptDep5.setId(5);
        fieldsOptDep5.setOptId("-LcpITCPz73aV7uQAx96");
        fieldsOptDep5.setOptNm("F2");
        new FieldsAsyncTask(context, fieldsOptDep5, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep6 = new FieldsOptDep();
        fieldsOptDep6.setfId("-LcpI8UEuMN1ByoRGPzN");
        fieldsOptDep6.setpId(0);
        fieldsOptDep6.setId(6);
        fieldsOptDep6.setOptId("-LcpI8Z3s9n1ISZSiB5J");
        fieldsOptDep6.setOptNm("AY");
        new FieldsAsyncTask(context, fieldsOptDep6, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep7 = new FieldsOptDep();
        fieldsOptDep7.setfId("-LcpILRiUI_ZschrgjLJ");
        fieldsOptDep7.setpId(6);
        fieldsOptDep7.setId(7);
        fieldsOptDep7.setOptId("-LcpILWZFhaV8OqZabAP");
        fieldsOptDep7.setOptNm("T3");
        new FieldsAsyncTask(context, fieldsOptDep7, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep8 = new FieldsOptDep();
        fieldsOptDep8.setfId("-LcpIT7bzVVg9CRvXx7F");
        fieldsOptDep8.setpId(7);
        fieldsOptDep8.setId(8);
        fieldsOptDep8.setOptId("-LcpITCSl59NEWQXRlpb");
        fieldsOptDep8.setOptNm("F5");
        new FieldsAsyncTask(context, fieldsOptDep8, FieldsOptDep.class.getName()).execute();

        FieldsOptDep fieldsOptDep9 = new FieldsOptDep();
        fieldsOptDep9.setfId("-LcpIT7bzVVg9CRvXx7F");
        fieldsOptDep9.setpId(7);
        fieldsOptDep9.setId(9);
        fieldsOptDep9.setOptId("-LcpITCRS3QAz8nhgGSB");
        fieldsOptDep9.setOptNm("F4");
        new FieldsAsyncTask(context, fieldsOptDep9, FieldsOptDep.class.getName()).execute();

        FieldsOptDep fieldsOptDep10 = new FieldsOptDep();
        fieldsOptDep10.setfId("-M-osVMcs1vsyfFkvxYO");
        fieldsOptDep10.setpId(3);
        fieldsOptDep10.setId(10);
        fieldsOptDep10.setOptId("-M-osVSoVZrDSN3tIvMT");
        fieldsOptDep10.setOptNm("Staff");
        new FieldsAsyncTask(context, fieldsOptDep10, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep11 = new FieldsOptDep();
        fieldsOptDep11.setfId("-M-osVMcs1vsyfFkvxYO");
        fieldsOptDep11.setpId(3);
        fieldsOptDep11.setId(11);
        fieldsOptDep11.setOptId("-M-osVSpv-b29njLyeq");
        fieldsOptDep11.setOptNm("Non Staff");
        new FieldsAsyncTask(context, fieldsOptDep11, FieldsOptDep.class.getName()).execute();




        FieldsOptDep fieldsOptDep12 = new FieldsOptDep();
        fieldsOptDep12.setfId("p");
        fieldsOptDep12.setpId(10);
        fieldsOptDep12.setId(12);
        fieldsOptDep12.setOptId("xyz");
        fieldsOptDep12.setOptNm("Testing");
        new AgentAsyncTask(context, fieldsOptDep12, FieldsOptDep.class.getName()).execute();


        FieldsOptDep fieldsOptDep13 = new FieldsOptDep();
        fieldsOptDep13.setfId("host");
        fieldsOptDep13.setpId(1);
        fieldsOptDep13.setId(13);
        fieldsOptDep13.setOptId("yyy");
        fieldsOptDep13.setOptNm("Alka");
        new FieldsAsyncTask(context, fieldsOptDep13, FieldsOptDep.class.getName()).execute();




        FieldsOptDep fieldsOptDep14 = new FieldsOptDep();
        fieldsOptDep14.setfId("p");
        fieldsOptDep14.setpId(11);
        fieldsOptDep14.setId(14);
        fieldsOptDep14.setOptId("xyz");
        fieldsOptDep14.setOptNm("Dev");
        new AgentAsyncTask(context, fieldsOptDep14, FieldsOptDep.class.getName()).execute();


     */


    }


    public void saveScreenData(final DatabaseReference scrnDb) {
        scrnDb.keepSynced(true);
        scrnDb.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if (dataSnapshot.exists()) {
                    SharedPreferences sp = context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt(GlobalVal.SCREEN_COUNT, Integer.parseInt(dataSnapshot.getChildrenCount() + "")).apply();
                    int i = 1;
                    for (DataSnapshot screenSnap : dataSnapshot.getChildren()) {
                        Screen screen = new Screen();
                        screen.setFormId(dataSnapshot.getKey());
                        screen.setId(Integer.parseInt(screenSnap.getKey()));
                        if (screenSnap.hasChild("title")) {
                            screen.setTitle(screenSnap.child("title").getValue(String.class));
                            editor.putString("screen" + screen.getId(), screenSnap.child("title").getValue(String.class)).apply();
                        }
                        new FieldsAsyncTask(context, screen, Screen.class.getName(), "INSERT").execute();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}


