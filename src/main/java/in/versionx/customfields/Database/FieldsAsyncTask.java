package in.versionx.customfields.Database;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.App;
import in.versionx.customfields.Interface.FieldsAsyncTaskListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptDep;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.Model.Screen;


public class FieldsAsyncTask extends AsyncTask<Void, Void, Boolean> {

    //Prevent leak
    private Context context;
    private Fields customFields;
    private FieldsOptions fieldsOptions;
    Screen screen;
    FieldsOptDep fieldsOptDep;
    String table;
    String action;


    public FieldsAsyncTask(Context context, Fields customFields, String table, String action) {
        this.context = context;
        this.customFields = customFields;
        this.table = table;
        this.action = action;
    }

    public FieldsAsyncTask(Context context, FieldsOptions fieldsOptions, String table, String action) {
        this.context = context;
        this.fieldsOptions = fieldsOptions;
        this.table = table;
        this.action = action;

    }


    public FieldsAsyncTask(Context context, FieldsOptDep fieldsOptDep, String table) {
        this.context = context;
        this.fieldsOptDep = fieldsOptDep;
        this.table = table;
    }


    public FieldsAsyncTask(Context context, Screen screen, String table, String action) {
        this.context = context;
        this.screen = screen;
        this.table = table;
        this.action = action;
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        if (table.equals(Fields.class.getName())) {
            if (action.equals("INSERT")) {

                int i = App.getDbInstance(context).fieldDao().getFieldById(customFields.getId()).size();

                if (i == 0)
                    App.getDbInstance(context).fieldDao().insertAll(customFields);
                else
                    App.getDbInstance(context).fieldDao().update(customFields);
            } else if (action.equals("UPDATE")) {
                App.getDbInstance(context).fieldDao().update(customFields);
            } else if (action.equals("DEL")) {
                App.getDbInstance(context).fieldDao().delete(customFields.getId());
            }
        }

        if (table.equals(FieldsOptions.class.getName())) {

            if (action.equals("INSERT")) {

                int i = App.getDbInstance(context).fieldOptionsDao().getItemId(fieldsOptions.getOptionId()).size();

                if (i == 0)
                    App.getDbInstance(context).fieldOptionsDao().insertAll(fieldsOptions);
                else
                    App.getDbInstance(context).fieldOptionsDao().update(fieldsOptions);
            } else if (action.equals("UPDATE")) {
                App.getDbInstance(context).fieldOptionsDao().update(fieldsOptions);
            } else if (action.equals("DEL")) {
                App.getDbInstance(context).fieldOptionsDao().delete(fieldsOptions.getOptionId());
            }
        }


        if (table.equals(FieldsOptDep.class.getName())) {

            List<FieldsOptDep> fieldsOptDeps = App.getDbInstance(context).fieldsDepDao().checkIfExits(fieldsOptDep.getId());
            if (fieldsOptDeps != null && fieldsOptDeps.size() > 0) {

                App.getDbInstance(context).fieldsDepDao().update(fieldsOptDep);

            } else {
                App.getDbInstance(context).fieldsDepDao().insertAll(fieldsOptDep);
            }

        }

        if (table.equals(Screen.class.getName())) {
            if (action.equals("INSERT")) {
                int i = App.getDbInstance(context).screenDao().getFieldsByScreenId(screen.getFormId(), screen.getId()).size();
                if (i == 0)
                    App.getDbInstance(context).screenDao().insertAll(screen);
                else
                    App.getDbInstance(context).screenDao().update(screen);
            } else if (action.equals("UPDATE")) {
                App.getDbInstance(context).screenDao().update(screen);
            }
                /* else if (action.equals("DEL")) {
                    App.getDbInstance(context).screenDao().delete(screen.getOptionId());
                }*/
        }

        return true;
    }
}

