package in.versionx.customfields.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import in.versionx.customfields.Model.FieldsOptions;


@Dao
public interface FieldOptionsDao {

    @Query("SELECT * FROM fieldsoptions WHERE optionId = :optId LIMIT 1")
    List<FieldsOptions> getItemId(String optId);
    @Insert
    void insertAll(FieldsOptions... fields);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(FieldsOptions... fields);


    @Query("SELECT * FROM FieldsOptions WHERE `fId`=:fId")
    List<FieldsOptions> getOptionsbyField(String fId);

    @Query("SELECT * FROM FieldsOptions WHERE `optionId` IN (:ids)")
    List<FieldsOptions> getDependencyOptions(String[] ids);



    @Query("SELECT * FROM FieldsOptions")
    List<FieldsOptions> getAll();


    @Query("DELETE  FROM FieldsOptions WHERE `optionId` =:optId")
    void delete(String optId);
}
