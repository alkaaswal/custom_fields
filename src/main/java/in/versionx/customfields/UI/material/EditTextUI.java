package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;

import static in.versionx.customfields.UI.CustomFieldsUI.getCapsTextNumFilter;

public class EditTextUI extends LinearLayout {


    private final View v;
    private TextInputLayout textInputLayout;
    // private ImageView clrImg;
    private EditText editText;
    String hintColor, textColor, underlineColor, textSize;
    EditTextUI editTextUI;
    LinearLayout ll;
    Fields field;
    Object text;
    HashMap<String, Object> textInHashMap;


    public EditTextUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.float_label_edit_text, this);


        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.underlineColor = ta.getString(R.styleable.CustomFieldsUI_underlineColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);

        initUI(v);


    }


    public void setInstance(EditTextUI editTextUI) {
        this.editTextUI = editTextUI;
    }

    public EditTextUI getInstance() {
        return this.editTextUI;
    }


    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public void setUnderlineColor(String underlineColor) {
        this.underlineColor = underlineColor;
    }

    public String getUnderlineColor() {
        return underlineColor;
    }

    public String getHintColor() {
        return hintColor;
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setField(Fields fields) {

        fields.setUiInstance(this);
        //  fields.setClrImg(clrImg);
        fields.setView(v);
        textInputLayout.setHint(fields.getName());
    /*    ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor(hintColor));
        textInputLayout.setDefaultHintTextColor(colorStateList);
        textInputLayout.setBackgroundTintList(colorStateList);*/
        editText.setTextSize(GlobalVal.getTextSize(textSize));
        editText.setTag(fields);
        editText.setText(fields.getVal());
        editText.setTextColor(Color.parseColor(textColor));
        editText.setHintTextColor(Color.parseColor(hintColor));
    //    editText.setBackgroundTintList(colorStateList);

        if (fields.getId().equalsIgnoreCase(GlobalVal.VEHICLE)) {
            editText.setFilters(new InputFilter[]{getCapsTextNumFilter()});
        }

        if (fields.getType().equalsIgnoreCase(GlobalVal.NUMBER)
                || fields.getType().equalsIgnoreCase(GlobalVal.DECIMAL_NUMBER)) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);

            if (fields.getType().equalsIgnoreCase(GlobalVal.DECIMAL_NUMBER)) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            }
            if (fields.getName().equalsIgnoreCase(GlobalVal.COUNT_FIELD_ID)) {
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
            } else {
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            }
        }
        if (fields.getType().equalsIgnoreCase(GlobalVal.TEXTBOX)) {
            editText.setSingleLine(true);
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        } else if (fields.getType().equals(GlobalVal.TEXTAREA)) {
            editText.setSingleLine(false);
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            editText.setGravity(Gravity.TOP | Gravity.LEFT);
            editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            editText.setLines(3);
            editText.setMaxLines(5);
        }


        if (fields.isHide() || !fields.isDsbl()) {
            v.setVisibility(View.GONE);

        }
        if (fields.isDep()) {
            v.setVisibility(View.GONE);
        }


        fields.setFieldView(editText);

        this.field = fields;
    }

    public Fields getField() {

        return this.field;

    }


    private void initUI(View v) {
        textInputLayout = (TextInputLayout) v.findViewById(R.id.float_label);
        //  clrImg = (ImageView) v.findViewById(R.id.iv_clear_ed);


        editText = (EditText) v.findViewById(R.id.editText);


    }


    public void setText(Object value) {

        if (value instanceof String) {
            this.text = (String) value;
            editText.setText((String) value);
        } else {
            HashMap<String, Object> val = (HashMap<String, Object>) value;
            editText.setText(val.get("val").toString());
            this.text = val.get("val").toString();
            textInHashMap = val;
        }


        field.setText(text.toString());

        editText.setError(null);


    }

    public HashMap<String, Object> getTextInHashMap() {
        return textInHashMap;
    }

    public String getText() {
        return editText.getText().toString().trim();
    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {


        if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())
                && !field.isHide()) {
            field.getView().setVisibility(View.VISIBLE);

        } else {
            setText("");
            field.getView().setVisibility(View.GONE);


        }

      /*  if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep() && options.size() == 0) {
                field.getView().setVisibility(View.GONE);
                editText.setText("");
                field.setText("");
            } else {
                field.getView().setVisibility(View.VISIBLE);
            }

        } else {
            if (field.isDep() && options.size() == 0) {
                field.getView().setVisibility(View.GONE);
                editText.setText("");
                field.setText("");
            }
        }*/
    }

    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {


            field.getView().setVisibility(View.GONE);


        }
    }


}
