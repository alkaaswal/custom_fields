package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.versionx.customfields.Adapter.BoxesAdapter;
import in.versionx.customfields.Adapter.FieldsAutoCompleteAdapter;
import in.versionx.customfields.Database.FieldDepAsync;
import in.versionx.customfields.Interface.OnItemSelectListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;
import in.versionx.customfields.UI.CustomFieldsUI;
import in.versionx.customfields.Utils.GlobalVal;

public class BoxUI extends LinearLayout implements BoxesAdapter.BoxItemSelected {


    View v;
    Context context;
    String hintColor, textColor, textSize;
    private RecyclerView recyclerView;
    private TextView box_heading;
    private FlexboxLayoutManager layoutManager;
    List<FieldsOptions> fieldsOptions;
    Fields field;
    OnItemSelectListner onItemSelectListner;

    public BoxUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.ll_for_boxes, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);

        initUI(v);
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }


    private void initUI(View v) {

        recyclerView = (RecyclerView) v.findViewById(R.id.rv_boxes);
        box_heading = (TextView) v.findViewById(R.id.box_heading);
        layoutManager = new FlexboxLayoutManager(context);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        recyclerView.setLayoutManager(layoutManager);

    }


    public void setField(Fields fields) {
        fields.setUiInstance(this);
        fields.setView(v);
        fields.setFieldView(recyclerView);
        box_heading.setTextSize(GlobalVal.getTextSize(textSize));
        box_heading.setText(fields.getName());
        if (fields.isHide() || (fields.isDep())) {
            v.setVisibility(View.GONE);

        }
        this.field = fields;
    }

    public Fields getField() {
        return field;
    }

    public void setFieldsOptions(List<FieldsOptions> options) {


        fieldsOptions = new ArrayList<>();
        for (int j = 0; j < options.size(); j++) {
            if (field.getId().equals(options.get(j).getfId())) {
                //  opts.add(options.get(j).getName());
                FieldsOptions fieldsOption = new FieldsOptions();
                fieldsOption.setfId(options.get(j).getfId());
                fieldsOption.setOptionId(options.get(j).getOptionId());
                fieldsOption.setName(options.get(j).getName());
                fieldsOption.setTyp(options.get(j).getTyp());
                if (options.size() == 1)
                    fieldsOption.setSelected(true);
                else {
                    if (field.getText().isEmpty())
                        fieldsOption.setSelected(options.get(j).isSelected);
                    else if (fieldsOption.getName().equals(field.getText()))
                        fieldsOption.setSelected(true);


                }
                fieldsOption.setpId(options.get(j).getpId());

                //  opt.add(fieldsOptions);
                fieldsOptions.add(fieldsOption);


                //  field.getView().setVisibility(VISIBLE);


            }
        }
        if (field.getOptions() == null) {

            ArrayList<FieldsOptions> arrlistofOptions = new ArrayList<FieldsOptions>(fieldsOptions);
            field.setOptions(arrlistofOptions);
        }


        setAdapter(fieldsOptions);


        /* this.fieldsOptions = fieldsOptions;*/
    }

    public List<FieldsOptions> getFieldsOptions() {
        return fieldsOptions;
    }

    public void setAdapter(List<FieldsOptions> options) {

        if (fieldsOptions.size() != 0) {

            BoxesAdapter boxesAdapter = new BoxesAdapter(context, options, this, field);
            RecyclerView rv = (RecyclerView) field.getFieldView();
            rv.setAdapter(boxesAdapter);
        }


    }

    public void setSelectedBox(Object value) {

        String text = "";


        if (value instanceof String) {

            text = value.toString();


        } else {
            try {
                HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
                for (Map.Entry<String, Object> entry : val.entrySet()) {
                    HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();


                    text = valEntry.get("val").toString();


                    break;
                }


            } catch (ClassCastException e) {
                try {
                    HashMap val = (HashMap) value;
                    ArrayList<Object> vals = (ArrayList) val.get("val");
                    HashMap<String, Object> hashMap = (HashMap) vals.get(0);
                    text = hashMap.get("val").toString();
                } catch (Exception e1) {

                }


            }
        }

        for (FieldsOptions options : fieldsOptions) {

            if (options.getName().equals(text)) {
                options.setSelected(true);
                field.setText(text.toString());
                  onItemSelectListner.onItemClick(options, field);
                break;
            }

        }



    }

    @Override
    public void onBoxItemSelected(List<FieldsOptions> fieldsOptions) {


        for (FieldsOptions options : fieldsOptions) {

            if (options.isSelected()) {
                field.setText(options.getName());
                onItemSelectListner.onItemClick(options, field);
                break;
            }


        }

    }


    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {
        /*if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep()) {
                field.getView().setVisibility(View.GONE);
            } else {
                field.getView().setVisibility(View.VISIBLE);

            }
        }
*/

        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {
            if (depFieldsId.size() == 0) {
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);
        } else {

        }


    }

    public void setListner(OnItemSelectListner onItemClickListner) {
        this.onItemSelectListner = onItemClickListner;


    }


    public void clearData() {
        fieldsOptions.clear();
        for (int i = 0; i < field.options.size(); i++) {
            field.options.get(i).setSelected(false);
        }
        fieldsOptions.addAll(field.options);
        setAdapter(fieldsOptions);
    }


    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

           // field.setText("");
            fieldsOptions.clear();
            field.getView().setVisibility(GONE);

                onItemSelectListner.onItemClear(field.getId());



        }
    }
}
