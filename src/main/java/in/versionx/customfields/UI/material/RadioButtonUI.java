package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.versionx.customfields.Interface.OnItemSelectListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;
import in.versionx.customfields.Utils.GlobalVal;

public class RadioButtonUI extends LinearLayout {

    View v;
    Context context;
    String hintColor, textColor, textSize;
    private RadioGroup radioGroup;
    TextView checkText;
    List<FieldsOptions> fieldsOptions;
    Fields field;
    OnItemSelectListner onItemSelectListner;

    public RadioButtonUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.ll_for_radio_group, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);

        initUI(v);
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public String getHintColor() {
        return hintColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }


    private void initUI(View v) {


        radioGroup = v.findViewById(R.id.radioGrp);
        checkText = v.findViewById(R.id.radio_txt);

    }


   /* public Fields getView(Fields fields)
    {
        fields.setView(v);
        checkText.setText(fields.getName());
        radioGroup.setTag(fields);

        if (fields.isHide() || !fields.isDsbl()) {
            v.setVisibility(View.GONE);
        }

        if (fields.isDep()) {
            radioGroup.setEnabled(false);
        }

        fields.setFieldView(radioGroup);

        return  fields;
    }*/

    public void setField(Fields fields) {
        fields.setUiInstance(this);
        fields.setView(v);
        checkText.setTextSize(GlobalVal.getTextSize(textSize));
        checkText.setText(fields.getName());
        radioGroup.setTag(fields);


        if (fields.isHide()) {
            v.setVisibility(View.GONE);
        }

        if (fields.isDep()) {
            v.setVisibility(View.GONE);
        }

        fields.setFieldView(radioGroup);


        this.field = fields;
    }

    public Fields getField() {
        return field;
    }


    public void setFieldsOptions(List<FieldsOptions> options) {

        //        radioGroup.removeAllViews();
        fieldsOptions = new ArrayList<>();

        for (int j = 0; j < options.size(); j++) {
            if (field.getId().equals(options.get(j).getfId())) {
                //  opts.add(options.get(j).getName());
                FieldsOptions fieldsOption = new FieldsOptions();
                fieldsOption.setfId(options.get(j).getfId());
                fieldsOption.setOptionId(options.get(j).getOptionId());
                fieldsOption.setName(options.get(j).getName());
                fieldsOption.setTyp(options.get(j).getTyp());
                if (options.size() == 1) {
                    fieldsOption.setSelected(true);

                    onItemSelectListner.onItemClick(fieldsOption, field);
                } else
                    fieldsOption.setSelected(false);

                //  opt.add(fieldsOptions);
                fieldsOptions.add(fieldsOption);


            }
        }
        for (FieldsOptions rbopt : fieldsOptions) {

            RadioButton radioButton = new RadioButton(context);
            radioButton.setTag(field);
            //  radioButton.setTextColor(Color.parseColor("#606060"));
            radioButton.setTextColor(Color.parseColor(textColor));
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 48);
            radioButton.setText(rbopt.getName());
            radioGroup.addView(radioButton);
        }

        if (field.getOptions() == null) {
            ArrayList<FieldsOptions> arrlistofOptions = new ArrayList<FieldsOptions>(fieldsOptions);
            field.setOptions(arrlistofOptions);
        }


    }

    public List<FieldsOptions> getFieldsOptions() {


        return fieldsOptions;
    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {

        radioGroup.removeAllViews();


        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            if (depFieldsId.size() == 0) {
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);
        }



       /* if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep()  || (field.isDep() && options.size() == 0)) {
                field.getView().setVisibility(View.GONE);
            } else {
                field.getView().setVisibility(View.VISIBLE);
            }

        }*/
    }

    public void setListner(OnItemSelectListner onItemSelectListners) {
        this.onItemSelectListner = onItemSelectListners;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);

                if (radioButton != null) {

                    Fields fields1 = (Fields) radioButton.getTag();

                    ArrayList<FieldsOptions> fieldsOptions = fields1.getOptions();

                    //  FieldsOptions fieldsOption = null;

                    for (FieldsOptions opts : fieldsOptions) {
                        if (opts.name.trim().equals(radioButton.getText().toString())) {
                            // fieldsOption = opts;
                            onItemSelectListner.onItemClick(opts, field);
                            break;
                        }

                    }
                }

            }
        });

    }


    public void clearData() {
        radioGroup.clearCheck();
        removeError();
        field.setText("");
        for (FieldsOptions opt : field.getOptions()) {

            opt.setSelected(false);
        }
    }

    public void setSelected(Object value) {
        int i = 0;
        String text = "";

        if (value instanceof String) {
            text = (String) value;
        } else {
            try {
                HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
                for (Map.Entry<String, Object> entry : val.entrySet()) {
                    HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();
                    text = valEntry.get("val").toString();
                    break;
                }


            } catch (ClassCastException e) {
                try {
                    HashMap val = (HashMap) value;
                    ArrayList<Object> vals = (ArrayList) val.get("val");
                    HashMap<String, Object> hashMap = (HashMap) vals.get(0);
                    text = hashMap.get("val").toString();

                } catch (Exception e1) {

                }


            }
        }

        for (FieldsOptions opt : field.getOptions()) {
            if (opt.getName().equals(text)) {
                ((RadioButton) radioGroup.getChildAt(i)).setChecked(true);
                field.setText(opt.getName());
                onItemSelectListner.onItemClick(opt, field);
                break;
            }
            i++;
        }
    }

    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {


            field.getView().setVisibility(View.GONE);


        }
    }

    public void setError() {
        checkText.setTextColor(Color.RED);

    }

    public void removeError() {
        checkText.setTextColor(Color.parseColor(textColor));
    }

}

