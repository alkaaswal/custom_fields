package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.R;

public class SeperationUI extends LinearLayout {



    View v;
    Context context;
    String hintColor, textColor;
    private TextView septext;

    public SeperationUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v =  inflater.inflate(R.layout.separation_layout, this);
        this.context=context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);

        initUI(v);
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    private void initUI(View v)
    {

         septext = (TextView) v.findViewById(R.id.tv_sep);

    }


    public Fields getView(Fields fields)
    {

        fields.setView(v);
        //    fieldInfo.setFieldView(imgtext);

        septext.setText(fields.getName().toUpperCase());
        septext.setTextColor(Color.parseColor(textColor));


                   /* if (fieldInfo.getFieldGrp().equals(Global.SCREEN_TYPE_WHOCAME))
                        llForCustomFields_Visitor.addView(root);
                    else
                        llForCustomFields_ToMeet.addView(root);*/

        if (fields.isHide() ) {
            septext.setVisibility(View.GONE);

        }

        fields.setFieldView(septext);

      return fields;
    }
}
