package in.versionx.customfields.UI.material;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;
import in.versionx.customfields.UI.custom.EditTextUI_custom;
import in.versionx.customfields.Utils.GlobalVal;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

public class DatePickerUI extends LinearLayout {
    private final View v;
    private TextFieldBoxes textInputLayout;
    private EditText editText;
    String hintColor, textColor, panelColor, textSize;
    Drawable drawableright;
    EditTextUI_custom editTextUI;
    Fields field;
    Object text;
    HashMap<String, Object> textInHashMap;
    Context context;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener date;

    public DatePickerUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.date_picker_ui, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);

        TypedArray editTa = context.obtainStyledAttributes(attrs, R.styleable.edittext, 0, 0);

        drawableright = editTa.getDrawable(R.styleable.edittext_drawableRight);


        initUI(v);
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setInstance(EditTextUI_custom editTextUI) {
        this.editTextUI = editTextUI;
    }

    public EditTextUI_custom getInstance() {
        return this.editTextUI;
    }


    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public String getHintColor() {
        return hintColor;
    }


    public void setField(Fields fields) {

        fields.setUiInstance(this);
        fields.setView(v);
        textInputLayout.setLabelText(fields.getName());

        editText.setTextSize(GlobalVal.getTextSize(textSize));
        editText.setTag(fields);
        editText.setText(fields.getVal());

        editText.setHint(fields.getVal());

        textInputLayout.setPrimaryColor(Color.parseColor(hintColor));
        if (panelColor != null)
            textInputLayout.setPanelBackgroundColor(Color.parseColor(panelColor));
        editText.setTextColor(Color.parseColor(textColor));


        if (fields.isHide()) {
            v.setVisibility(View.GONE);

        }
        if (fields.isDep()) {
            v.setVisibility(View.GONE);
        }


        fields.setFieldView(editText);

        this.field = fields;
    }

    public Fields getField() {

        return this.field;

    }


    private void initUI(View v) {


        editText = (EditText) v.findViewById(R.id.editText);
        textInputLayout = (TextFieldBoxes) v.findViewById(R.id.float_label);

        textInputLayout = (TextFieldBoxes) v.findViewById(R.id.float_label);
        if (drawableright == null) {
            textInputLayout.setEndIcon(R.drawable.ic_calender);
        } else {
            textInputLayout.setEndIcon(drawableright);
        }

        calendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        editText.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    //CREATE YOUR DATE PICKER DIALOG HERE.
                    openDatePickerDialog(view);
                }
                return false;
            }
        });

        /*editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                openDatePickerDialog(view);

            }
        });
        editText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePickerDialog(view);


            }
        });*/

    }

    private void updateLabel() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        editText.setText(sdf.format(calendar.getTime()));
    }


    public void setText(Object value) {

        if (value instanceof String) {
            this.text = (String) value;
            editText.setText((String) value);
        } else {
            HashMap<String, Object> val = (HashMap<String, Object>) value;

            editText.setText(val.get("val").toString());
            this.text = val.get("val").toString();
            textInHashMap = val;


        }


    }

    public HashMap<String, Object> getTextInHashMap() {
        return textInHashMap;
    }

    public Object getText() {
        return editText.getText().toString().trim();
    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {
      /*  if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep()  ||(field.isDep() && options.size()==0)) {
                field.getView().setVisibility(View.GONE);
            } else {
                field.getView().setVisibility(View.VISIBLE);
            }

        }*/


        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            if (depFieldsId.size() == 0) {
                editText.setText("");
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);
        } else {
            //  field.getView().setVisibility(View.GONE);


        }
    }


    public void openDatePickerDialog(final View v) {
        // Get Current Date
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        Calendar cal = Calendar.getInstance();
        new DatePickerDialog(context, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {


            field.getView().setVisibility(View.GONE);


        }
    }

    public void setError(String msg) {
        textInputLayout.setError(msg, false);
    }

    public void removeError() {
        textInputLayout.removeError();
    }

    public void clearData()
    {
        setText("");
        removeError();

    }
}
