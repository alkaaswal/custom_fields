package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.versionx.customfields.Adapter.FieldsAutoCompleteAdapter;
import in.versionx.customfields.AutoCompleteTextViewClickListener;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Interface.OnItemSelectListner;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;

import static in.versionx.customfields.UI.CustomFieldsUI.getCapsTextNumFilter;

public class AutoCompleteTextViewUI extends LinearLayout {

    View v;
    String hintColor, textColor, textSize;
    Object text;
    Context context;
    private TextInputLayout textInputLayout;

    private AutoCompleteTextView autoCompleteTextView;
    Fields field;
    List<FieldsOptions> fieldsOptions;
    OnItemSelectListner onItemSelectListner;

    HashMap<String, Object> textInHashMap;

    public AutoCompleteTextViewUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.float_label_autocompletetext, this);
        this.context = context;
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);
        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);
        initUI(v);
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public String getHintColor() {
        return hintColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }

    private void initUI(View v) {
        textInputLayout = (TextInputLayout) v.findViewById(R.id.autocomplete_floatLable);

        autoCompleteTextView = (AutoCompleteTextView) v.findViewById(R.id.autocompletetxt);
        fieldsOptions = new ArrayList<>();
        //     fieldInfo.setFieldView(autoCompleteTextView);

    }

    public void setFieldsOptions(List<FieldsOptions> options) {

        fieldsOptions.clear();
        for (int j = 0; j < options.size(); j++) {
            if (field.getId().equals(options.get(j).getfId())) {
                //  opts.add(options.get(j).getName());
                FieldsOptions fieldsOption = new FieldsOptions();
                fieldsOption.setfId(options.get(j).getfId());
                fieldsOption.setOptionId(options.get(j).getOptionId());
                fieldsOption.setName(options.get(j).getName());
                fieldsOption.setTyp(options.get(j).getTyp());


                //  fieldsOption.setSelected(options.get(j).isSelected());
                fieldsOption.setpId(options.get(j).getpId());

                if (options.size() == 1) {
                    fieldsOption.setSelected(true);
                } else
                    fieldsOption.setSelected(false);
                fieldsOptions.add(fieldsOption);


            }
        }
        if (field.getOptions() == null) {
            ArrayList<FieldsOptions> arrlistofOptions = new ArrayList<FieldsOptions>(fieldsOptions);
            field.setOptions(arrlistofOptions);
        }

        if (fieldsOptions != null && fieldsOptions.size() > 0 && field.getModLbl() == null) {
            if (fieldsOptions.size() > 15) {
                autoCompleteTextView.setThreshold(3);
                setAdapter(fieldsOptions);
            } else {
                autoCompleteTextView.setThreshold(0);
                setAdapter(fieldsOptions);
            }
        }


    }


    public void setAdapter(List<FieldsOptions> options) {

        FieldsAutoCompleteAdapter adapter = null;

        if (textSize.equals("N"))
            adapter = new FieldsAutoCompleteAdapter(context, android.R.layout.simple_dropdown_item_1line, options);
        else
            adapter = new FieldsAutoCompleteAdapter(context, R.layout.dropdown_textview, options);

        if (autoCompleteTextView != null && options != null && options.size() > 0) {

            autoCompleteTextView.setAdapter(adapter);

            if (options.size() == 1) {

              autoCompleteTextView.setText(options.get(0).getName());

                onItemSelectListner.onItemClick(options.get(0), field);
            }

        }
    }

    public List<FieldsOptions> getFieldsOptions() {
        return fieldsOptions;
    }

    public void setField(Fields fields) {

        fields.setUiInstance(this);

        fields.setView(v);

        /*ColorStateList colorStateList = ColorStateList.valueOf(Color.parseColor(hintColor));
        textInputLayout.setDefaultHintTextColor(colorStateList);
        textInputLayout.setBackgroundTintList(colorStateList);*/
        textInputLayout.setHint(fields.getName());

        autoCompleteTextView.setTextSize(GlobalVal.getTextSize(textSize));
        if (fields.getId().equalsIgnoreCase(GlobalVal.VEHICLE)) {
            autoCompleteTextView.setFilters(new InputFilter[]{getCapsTextNumFilter()});
        }

        if (fields.isSrch()) {
            autoCompleteTextView.setFocusable(true);
            autoCompleteTextView.setCursorVisible(true);
            autoCompleteTextView.setThreshold(3);
            //  autoCompleteTextView.setThreshold(1);

        } else {
            autoCompleteTextView.setFocusable(false);
            autoCompleteTextView.setCursorVisible(false);
            autoCompleteTextView.setThreshold(256);
            autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (autoCompleteTextView.isEnabled()) {
                        autoCompleteTextView.showDropDown();
                        autoCompleteTextView.requestFocus();
                    }
                    return false;

                }
            });

        }

        autoCompleteTextView.setTextColor(Color.parseColor(textColor));
        autoCompleteTextView.setHintTextColor(Color.parseColor(hintColor));
      //  autoCompleteTextView.setBackgroundTintList(colorStateList);
        autoCompleteTextView.setText(fields.getVal());
        autoCompleteTextView.setTag(fields);


        if (fields.isHide() || fields.isDep()) {

            v.setVisibility(View.GONE);

        }


        fields.setFieldView(autoCompleteTextView);


        this.field = fields;
    }

    public Fields getField() {

        return this.field;
    }

    public void setText(Object value) {

        if (value instanceof String) {
            this.text = (String) value;
            autoCompleteTextView.setText((String) value);
        } else {
            try {
                HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
                for (Map.Entry<String, Object> entry : val.entrySet()) {
                    HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();
                    this.textInHashMap = valEntry;
                    autoCompleteTextView.setText(valEntry.get("val").toString());
                    this.text = valEntry.get("val").toString();
                    break;
                }


            } catch (ClassCastException e) {
                try {
                    HashMap val = (HashMap) value;
                    ArrayList<Object> vals = (ArrayList) val.get("val");
                    HashMap<String, Object> hashMap = (HashMap) vals.get(0);
                    this.textInHashMap = val;
                    this.text = hashMap.get("val");
                    autoCompleteTextView.setText(hashMap.get("val").toString());
                } catch (Exception e1) {

                }


            }
        }


        field.setText(text.toString());


        for (FieldsOptions options : fieldsOptions) {

            if (options.getName().equals(text.toString())) {
                onItemSelectListner.onItemClick(options, field);
                break;
            }
        }


    }


    public HashMap<String, Object> getTextInHashMap() {
        return this.textInHashMap;
    }

    public HashMap<String, Object> getSelectedOption() {

        HashMap<String, Object> hashMap = new HashMap<>();
        for (FieldsOptions options : field.getOptions()) {
            if (autoCompleteTextView.getText().toString().trim().equals(options.getName())) {
                hashMap.put(options.getOptionId(), options.getName());
            }
        }


        return hashMap;
    }


    public String getText() {

        return autoCompleteTextView.getText().toString().trim();
    }

    public void setError(String err) {
        if (err == null) {
            autoCompleteTextView.setError(null);
        } else {
            autoCompleteTextView.setError(err);
        }


    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {


        if (depFieldsId != null && depFieldsId.contains(field.getId())) {

            if (depFieldsId.size() == 0) {
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);

        }





        /*if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep() && options.size() == 0) {
                autoCompleteTextView.setText("");
                field.setText("");
                field.getView().setVisibility(View.GONE);
            } else {
                field.getView().setVisibility(View.VISIBLE);
            }
        }*/
    }

    public void setListner(OnItemSelectListner onItemClickListner) {
        this.onItemSelectListner = onItemClickListner;

        if (autoCompleteTextView.getOnItemClickListener() == null) {
            autoCompleteTextView.setOnItemClickListener(
                    new AutoCompleteTextViewClickListener(autoCompleteTextView, new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (parent.getItemAtPosition(position) instanceof FieldsOptions) {
                                FieldsOptions fieldsOptions = (FieldsOptions) parent.getItemAtPosition(position);

                                onItemSelectListner.onItemClick(fieldsOptions, field);
                            }
                        }
                    }));


            autoCompleteTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (editable.toString().trim().isEmpty()) {
                        onItemSelectListner.onItemClear(field.getId());
                    }

                }
            });


        }


    }


    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            autoCompleteTextView.setText("");
            field.getView().setVisibility(View.GONE);


        }
    }


}
