package in.versionx.customfields.UI.material;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;
import in.versionx.customfields.Utils.GlobalVal;

public class CheckBoxUI extends LinearLayout {


    View v;
    Context context;
    String hintColor, textColor, textSize;
    private TextView checkText;
    List<FieldsOptions> fieldsOptions;
    Fields field;
    ArrayList<CheckBox> checkBoxes;

    public CheckBoxUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.ll_for_check_box, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);

        initUI(v);
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public String getHintColor() {
        return hintColor;
    }

    private void initUI(View v) {


        checkText = v.findViewById(R.id.cb_txt);


    }

/*
    public Fields getView(Fields fields)
    {

        fields.setView(v);
        checkText.setText(fields.getName());
        if (fields.isHide() || !fields.isDsbl()) {
            v.setVisibility(View.GONE);
        }


      //  fields.setFieldView(checkText);


       return fields;
    }*/


    public void setField(Fields fields) {
        fields.setUiInstance(this);
        fields.setView(v);
        checkText.setTextColor(Color.parseColor(textColor));
        checkText.setTextSize(GlobalVal.getTextSize(textSize));
        checkText.setText(fields.getName());
        if (fields.isHide()) {
            v.setVisibility(View.GONE);
        }

        this.field = fields;
    }

    public Fields getField() {
        return field;
    }

    public void setFieldsOptions(List<FieldsOptions> options) {
        fieldsOptions = new ArrayList<>();
        for (int j = 0; j < options.size(); j++) {
            if (field.getId().equals(options.get(j).getfId())) {
                //  opts.add(options.get(j).getName());
                FieldsOptions fieldsOption = new FieldsOptions();
                fieldsOption.setfId(options.get(j).getfId());
                fieldsOption.setOptionId(options.get(j).getOptionId());
                fieldsOption.setName(options.get(j).getName());
                fieldsOption.setTyp(options.get(j).getTyp());

                if (field.getText().isEmpty())
                    fieldsOption.setSelected(options.get(j).isSelected);
                else if (fieldsOption.getName().equals(field.getText()))
                    fieldsOption.setSelected(true);


                //  opt.add(fieldsOptions);
                fieldsOptions.add(fieldsOption);


            }
        }

        if (field.getOptions() == null) {
            ArrayList<FieldsOptions> arrlistofOptions = new ArrayList<FieldsOptions>(fieldsOptions);
            field.setOptions(arrlistofOptions);
        }

        if (fieldsOptions != null && fieldsOptions.size() > 0)
            setAdapter(fieldsOptions);

    }

    public List<FieldsOptions> getFieldsOptions() {
        return fieldsOptions;
    }

    public ArrayList<CheckBox> getCheckBoxes() {
        return checkBoxes;
    }

    public void setCheckBoxes(ArrayList<CheckBox> checkBoxes) {
        this.checkBoxes = checkBoxes;
    }

    public void setAdapter(List<FieldsOptions> options) {
        checkBoxes = new ArrayList<>();
        LinearLayout ll_cb = v.findViewById(R.id.ll_cb);
        ll_cb.removeAllViews();
        int j = 0;
        for (FieldsOptions cbopt : options) {
            CheckBox checkBox = new CheckBox(context);
            checkBox.setTag(options);
            checkBox.setId(j);
            /* checkBox.setTextColor(Color.parseColor("#606060"));*/
            checkBox.setTextColor(Color.parseColor(textColor));
            checkBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, 48);
            checkBox.setText(cbopt.getName());
            checkBoxes.add(checkBox);
            ll_cb.addView(checkBox);
            j++;
        }
        field.setFieldView(ll_cb);
    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {
        /*if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep() || (field.isDep() && options.size()==0)) {
                field.getView().setVisibility(View.GONE);
            } else {
                field.getView().setVisibility(View.VISIBLE);

            }
        }*/


        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            if (depFieldsId.size() == 0) {
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);
        } else {
            //  field.getView().setVisibility(View.GONE);

        }

    }

    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            field.setText("");

            field.getView().setVisibility(View.GONE);


        }
    }

    public void setSelectedCheckBox(Object value) {

        String text = "";


        if (value instanceof String) {

            text = value.toString();


        } else {
            try {
                HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
                for (Map.Entry<String, Object> entry : val.entrySet()) {
                    HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();


                    text = valEntry.get("val").toString();


                    for (CheckBox checkBox : checkBoxes) {
                        if (checkBox.getText().toString().equals(text))
                            checkBox.setSelected(true);
                        checkBox.setChecked(true);
                        field.setText(text.toString());


                    }
                }


            } catch (ClassCastException e) {
                try {
                    HashMap val = (HashMap) value;
                    ArrayList<Object> vals = (ArrayList) val.get("val");
                    HashMap<String, Object> hashMap = (HashMap) vals.get(0);
                    text = hashMap.get("val").toString();

                    for (CheckBox checkBox : checkBoxes) {
                        if (checkBox.getText().toString().equals(text))
                            checkBox.setSelected(true);
                        checkBox.setChecked(true);
                        field.setText(text.toString());


                    }

                } catch (Exception e1) {

                }


            }
        }


    }

    public void clearData() {
        // field.setText("");

        for (CheckBox checkBox : checkBoxes) {
            checkBox.setSelected(false);
            checkBox.setChecked(false);

        }

    }

    public void setError() {
        checkText.setTextColor(Color.RED);
    }

    public void removeError() {



        checkText.setTextColor(Color.parseColor(textColor));


    }

}


