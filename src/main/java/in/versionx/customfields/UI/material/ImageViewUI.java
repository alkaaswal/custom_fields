package in.versionx.customfields.UI.material;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.versionx.customfields.Camera.CameraActivity;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.UI.CustomFieldsUI;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Model.Images;
import in.versionx.customfields.R;

public class ImageViewUI extends LinearLayout {

    View v;
    Context context;
    String hintColor, textColor, imageNm, imagePath, alignment;
    private TextView imgtext;
    private ImageView imageView;
    private LinearLayout ll_img;
    Fields field;
    Object image;
    boolean multi;
    RecyclerView rv_iv;
    Images images;
    String module;
    HashMap<String, Object> textInHashMap;
    boolean isFront;


    public ImageViewUI(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.customimg_layout, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.alignment = ta.getString(R.styleable.CustomFieldsUI_align);
        this.multi = ta.getBoolean(R.styleable.ImageViewUI_custom_multi, false);
        v = view;
        initUI(view);


    }

    public void setFront(boolean front) {
        isFront = front;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setImageNm(String imageNm) {
        this.imageNm = imageNm;
    }


    private void initUI(View v) {
        imgtext = (TextView) v.findViewById(R.id.tv_imgtxt);
        imageView = (ImageView) v.findViewById(R.id.iv_img);
        ll_img = (LinearLayout) v.findViewById(R.id.ll_img);
        rv_iv = (RecyclerView) v.findViewById(R.id.rv_iv);
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getModule() {
        return module;
    }

    public void setField(Fields fields) {

        fields.setUiInstance(this);
        fields.setView(v);


        if (fields.getId().equals(GlobalVal.VISITOR_ID)) {
            imageView.setImageResource(R.drawable.ic_id_card);

        } else
            imageView.setImageResource(R.drawable.ic_photo);


        if (fields.getId().equals("img") && module.equals("visitor")) {
            ll_img.setGravity(Gravity.CENTER);
            imageView.setImageResource(R.drawable.default_user_picture);
        } else {
            ll_img.setGravity(Gravity.LEFT);
            imageView.setImageResource(R.drawable.ic_photo);
        }

        imgtext.setTextColor(Color.parseColor(textColor));
        imageView.setTag(fields);


        imgtext.setText(fields.getName());

        if (fields.isHide() || fields.isDep()) {
            imgtext.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);

        }

        fields.setFieldView(imageView);


        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                Fields fieldInfo = (Fields) view.getTag();
                dispatchCamera(fieldInfo);

            }
        });


        this.field = fields;
    }

    public Fields getField() {
        return this.field;
    }

    public void setImage(Object value) {

        if (value instanceof String) {

            this.image = (String) value;
            Glide.with(context).load(image).asBitmap().override(200, 200).fitCenter().
                    diskCacheStrategy(DiskCacheStrategy.RESULT).
                    into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            imageView.setImageBitmap(resource);
                        }
                    });
        } else {
            HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
            for (Map.Entry<String, Object> entry : val.entrySet()) {
                HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();

                this.textInHashMap = valEntry;
                this.image = valEntry.get("val").toString();

                Glide.with(context).load(valEntry.get("val").toString()).asBitmap().override(200, 200).fitCenter().
                        diskCacheStrategy(DiskCacheStrategy.RESULT).
                        into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                imageView.setImageBitmap(resource);
                            }
                        });


                break;

            }


        }

    }

    public void setTextInHashMap(HashMap<String, Object> textInHashMap) {
        this.textInHashMap = textInHashMap;
    }

    public HashMap<String, Object> getTextInHashMap() {
        return textInHashMap;
    }

    public Object getImage() {
        return image;
    }

    private void dispatchCamera(Fields fieldsInfo) {

        boolean frontCam = context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE)
                .getBoolean(GlobalVal.FRONT_CAM, false);

        Intent intent = new Intent(context, CameraActivity.class);
        String imgPath = null;
        if (imageNm == null)
            imgPath = createFileFolder(new Date().getTime() + ".jpg");
        else
            imgPath = createFileFolder(imageNm + ".jpg");
        intent.putExtra("id", fieldsInfo.getId());
        intent.putExtra("front", frontCam);
        intent.putExtra("path", imgPath);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, 199);

    }

    // create folder
    public String createFileFolder(String filename) {
        File dir = null;
        if (imagePath == null) {
            dir = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Entry" + File.separator + "Images");
        } else {
            dir = new File(imagePath);
        }

        if (!dir.exists())
            dir.mkdirs();

        File outputFile = new File(dir, filename);


        return outputFile.getAbsolutePath();
    }


    public void setImageVal(Object value) {


        HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
        for (Map.Entry<String, Object> entry : val.entrySet()) {
            HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();
            Images images = new Images();
            images.setDt(entry.getKey());
            images.setFilePath(valEntry.get("val").toString());
            images.setFieldId(field.getId());


            Glide.with(context).load(valEntry.get("val").toString()).asBitmap().override(200, 200).fitCenter().
                    diskCacheStrategy(DiskCacheStrategy.RESULT).
                    into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            imageView.setImageBitmap(resource);
                        }
                    });


            this.images = images;
            break;


        }


    }


    public void setImages(Images images, boolean show) {

        if (show) {
            Glide.with(context).load(images.getFilePath()).asBitmap().override(200, 200).fitCenter().
                    diskCacheStrategy(DiskCacheStrategy.RESULT).
                    into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            imageView.setImageBitmap(resource);
                        }
                    });
        }


        this.images = images;
    }

    public Images getImages() {
        return images;
    }


    public ImageView getFieldView() {
        return imageView;
    }

    public TextView getImgtext() {
        return imgtext;
    }

    public void clearData() {


        if (field.getId().equals("img") && module.equals("visitor")) {
            imageView.setImageResource(0);
            imageView.setImageDrawable(null);
            imageView.setImageResource(R.drawable.default_user_picture);
        } else if (field.getId().equals(GlobalVal.VISITOR_ID)) {


            imageView.setImageResource(R.drawable.ic_id_card);


        } else {
            imageView.setImageResource(0);
            imageView.setImageDrawable(null);
            imageView.setImageResource(R.drawable.ic_photo);
        }

        Glide.clear(imageView);
        imgtext.setTextColor(Color.parseColor(textColor));

        if (images != null && !images.equals(null)) {
            images.setFilePath("");
            images.setDt("");
        }
        field.setVal("");
    }

    public void setError(String err) {
        if (err != null)
            imgtext.setTextColor(Color.RED);
        else
            imgtext.setHintTextColor(Color.parseColor(hintColor));

    }

}
