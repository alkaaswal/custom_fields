package in.versionx.customfields.UI.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.versionx.customfields.Adapter.ImageAdapter;
import in.versionx.customfields.Camera.CameraActivity;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.Images;
import in.versionx.customfields.R;
import in.versionx.customfields.UI.CustomFieldsUI;
import in.versionx.customfields.UI.material.ImageViewUI;
import in.versionx.customfields.Utils.GlobalVal;


public class ImageViewUI_custom extends LinearLayout {

    private final String panelColor;
    View v;
    Context context;
    String hintColor, textColor, imageNm, imagePath, alignment;
    //   private ImageView imageView;

    Fields field;
    boolean multi;
    HashMap<String, Object> textInHashMap;
    private ImageView iv;
    ArrayList<Images> images;
    RecyclerView rv_iv;


    private TextView tv_imgtxt;
    private LinearLayout ll_img, ll_img_background;
    private ImageAdapter imageAdapter;
    boolean isFront;

    public ImageViewUI_custom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.image_custom, this);
        this.context = context;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.alignment = ta.getString(R.styleable.CustomFieldsUI_align);
        this.imagePath = ta.getString(R.styleable.CustomFieldsUI_imgPath);
        this.panelColor = ta.getString(R.styleable.CustomFieldsUI_panelColor);
        this.multi = ta.getBoolean(R.styleable.ImageViewUI_custom_multi, false);
        initUI(v);
    }

    public void setFront(boolean front) {
        isFront = front;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setImageNm(String imageNm) {
        this.imageNm = imageNm;
    }


    private void initUI(View v) {


        iv = (ImageView) findViewById(R.id.image);
        tv_imgtxt = (TextView) findViewById(R.id.tv_imgtxt);
        ll_img = (LinearLayout) findViewById(R.id.ll_img);
        ll_img_background = (LinearLayout) findViewById(R.id.ll_img_background);
        rv_iv = (RecyclerView) findViewById(R.id.rv_iv);
        images = new ArrayList<>();


    }

    public void setField(Fields fields) {

        fields.setUiInstance(this);
        fields.setView(v);

        iv.setTag(fields);
        tv_imgtxt.setTextColor(Color.parseColor(hintColor));
        tv_imgtxt.setHintTextColor(Color.parseColor(hintColor));
        if (panelColor != null)
            ll_img_background.setBackgroundColor(Color.parseColor(panelColor));

        iv.setImageResource(R.drawable.ic_image_custom);


        tv_imgtxt.setText(fields.getName());

        if (fields.isHide() || fields.isDep()) {
            ll_img.setVisibility(View.GONE);

        }

        fields.setFieldView(iv);


        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // What you want to do when it's clicked

                Fields fieldInfo = (Fields) view.getTag();
                dispatchCamera(fieldInfo);
            }
        });

        if (isMulti()) {

            rv_iv.setVisibility(View.VISIBLE);
            int numberOfColumns = 3;
            rv_iv.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
            imageAdapter = new ImageAdapter(context, images);
            rv_iv.setAdapter(imageAdapter);
        } else {
            rv_iv.setVisibility(View.GONE);


        }


        this.field = fields;
    }

    public Fields getField() {
        return this.field;
    }

    public void setImageVal(Object value) {


        HashMap<String, Object> val = (HashMap<String, Object>) ((HashMap) value).get("val");
        for (Map.Entry<String, Object> entry : val.entrySet()) {
            HashMap<String, Object> valEntry = (HashMap<String, Object>) entry.getValue();
            Images images = new Images();
            images.setDt(entry.getKey());
            images.setFilePath(valEntry.get("val").toString());
            images.setFieldId(field.getId());


            if (!isMulti()) {
                Glide.with(context).load(valEntry.get("val").toString()).asBitmap().override(200, 200).fitCenter().
                        diskCacheStrategy(DiskCacheStrategy.RESULT).
                        into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                iv.setImageBitmap(resource);
                            }
                        });
                this.images.add(images);
                break;
            } else {
                this.images.add(images);
                imageAdapter.notifyDataSetChanged();
            }


        }

    }


    private void dispatchCamera(Fields fieldsInfo) {
        boolean frontCam = context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE)
                .getBoolean(GlobalVal.FRONT_CAM, false);

        Intent intent = new Intent(context, CameraActivity.class);
        String imgPath = null;
        if (imageNm == null)
            imgPath = createFileFolder(new Date().getTime() + ".jpg");
        else
            imgPath = createFileFolder(imageNm + ".jpg");
        intent.putExtra("id", fieldsInfo.getId());
        intent.putExtra("path", imgPath);
        intent.putExtra("front", frontCam);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, 199);

    }

    // create folder
    public String createFileFolder(String filename) {
        File dir = null;
        if (imagePath == null) {
            dir = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Entry" + File.separator + "Images");
        } else {
            dir = new File(imagePath);
        }

        if (!dir.exists())
            dir.mkdirs();

        File outputFile = new File(dir, filename);


        return outputFile.getAbsolutePath();
    }

    public void setImages(ArrayList<Images> images, boolean show) {

        if (isMulti()) {
            this.images.add(images.get(0));
            this.imageAdapter.notifyDataSetChanged();
        } else {

            if (show) {
                Glide.with(context).load(images.get(0).getFilePath()).asBitmap().fitCenter().override(200, 200).fitCenter().
                        diskCacheStrategy(DiskCacheStrategy.RESULT).
                        into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                                iv.setImageBitmap(resource);
                            }
                        });
            }
            this.images.add(images.get(0));
        }


    }

    public ArrayList<Images> getImages() {
        return images;
    }


    public ImageView getFieldView() {
        return iv;
    }


    public TextView getImgtext() {
        return tv_imgtxt;
    }


    public void clearData() {
        if (isMulti()) {

            this.imageAdapter.notifyDataSetChanged();

        } else {
            iv.setImageResource(R.drawable.ic_image_custom);

        }
        tv_imgtxt.setHintTextColor(Color.parseColor(hintColor));
        this.images.clear();
        field.setVal("");
    }

    public void setError(String err) {
        if (err != null)
            tv_imgtxt.setTextColor(Color.RED);
        else
            tv_imgtxt.setHintTextColor(Color.parseColor(hintColor));

    }

}
