package in.versionx.customfields.UI.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.bumptech.glide.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;
import studio.carbonylgroup.textfieldboxes.TextFieldBoxes;

import static in.versionx.customfields.UI.CustomFieldsUI.getCapsTextNumFilter;

public class EditTextUI_custom extends LinearLayout {


    private final View v;
    private TextFieldBoxes textInputLayout;
    private EditText editText;
    String hintColor, textColor, panelColor, textSize = "L";
    Drawable drawableright;
    EditTextUI_custom editTextUI;
    Fields field;
    Object text;

    HashMap<String, Object> textInHashMap;

    public EditTextUI_custom(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomFieldsUI, 0, 0);

        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.panelColor = ta.getString(R.styleable.CustomFieldsUI_panelColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);

        TypedArray editTa = context.obtainStyledAttributes(attrs, R.styleable.edittext, 0, 0);

        drawableright = editTa.getDrawable(R.styleable.edittext_drawableRight);


        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.edittext_custom, this);


        initUI(v);


    }


    public void setInstance(EditTextUI_custom editTextUI) {
        this.editTextUI = editTextUI;
    }

    public EditTextUI_custom getInstance() {
        return this.editTextUI;
    }


    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setHintColor(String hintColor) {
        this.hintColor = hintColor;
    }

    public String getHintColor() {
        return hintColor;
    }


    public void setField(Fields fields) {

        fields.setUiInstance(this);
        fields.setView(v);
        textInputLayout.setLabelText(fields.getName());


        editText.setTag(fields);
        editText.setText(fields.getVal());
        editText.setTextSize(GlobalVal.getTextSize(textSize));
        textInputLayout.setPrimaryColor(Color.parseColor(hintColor));
        if (panelColor != null)
            textInputLayout.setPanelBackgroundColor(Color.parseColor(panelColor));
        editText.setTextColor(Color.parseColor(textColor));
        if (fields.getId().equalsIgnoreCase(GlobalVal.VEHICLE)) {
            editText.setFilters(new InputFilter[]{getCapsTextNumFilter()});
        }

        if (fields.getType().equalsIgnoreCase(GlobalVal.NUMBER) || fields.getType().equalsIgnoreCase(GlobalVal.DECIMAL_NUMBER)) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);

            if (fields.getType().equalsIgnoreCase(GlobalVal.DECIMAL_NUMBER)) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            }
            if (fields.getName().equalsIgnoreCase(GlobalVal.COUNT_FIELD_ID)) {
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});

            } else {
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            }
        }
        if (fields.getType().equalsIgnoreCase(GlobalVal.TEXTBOX)) {
            editText.setSingleLine(true);
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        } else if (fields.getType().equals(GlobalVal.TEXTAREA)) {
            editText.setSingleLine(false);
            editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            editText.setGravity(Gravity.TOP | Gravity.LEFT);
            editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            editText.setLines(3);
            editText.setMaxLines(5);
        }


        if (fields.isHide()) {
            v.setVisibility(View.GONE);

        }
        if (fields.isDep()) {
            editText.setEnabled(false);
        }


        fields.setFieldView(editText);

        this.field = fields;
    }

    public Fields getField() {

        return this.field;

    }


    private void initUI(View v) {


        editText = (EditText) v.findViewById(R.id.editText);

        textInputLayout = (TextFieldBoxes) v.findViewById(R.id.float_label);


    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public void setText(Object value) {

        if (value instanceof String) {
            this.text = (String) value;
            editText.setText((String) value);
        } else {
            HashMap<String, Object> val = (HashMap<String, Object>) value;

            editText.setText(val.get("val").toString());
            this.text = val.get("val").toString();
            textInHashMap = val;


        }

        setError("");
        removeError();


    }

    public HashMap<String, Object> getTextInHashMap() {
        return textInHashMap;
    }

    public Object getText() {
        return editText.getText().toString().trim();
    }

    public void setDep(ArrayList<String> depFieldsId, List<FieldsOptions> options) {


        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {

            if (depFieldsId.size() == 0) {
                setText("");
                field.getView().setVisibility(View.GONE);
            } else
                field.getView().setVisibility(View.VISIBLE);


        } else {


            //   field.getView().setVisibility(View.GONE);


        }
       /* if (depFieldsId != null && depFieldsId.size() > 0 && depFieldsId.contains(field.getId())) {
            if (field.isDep() && options.size()==0) {
                field.getView().setVisibility(View.GONE);
            } else {
                field.getFieldView().setVisibility(View.VISIBLE);
            }

        }*/
    }

    public void clearDep(ArrayList<String> depFieldsId) {
        if (depFieldsId != null && depFieldsId.contains(field.getId()) && !field.isHide()) {


            field.getView().setVisibility(View.GONE);


        }
    }

    public void setError(String msg) {
        textInputLayout.setError(msg, false);

    }

    public void removeError() {
        textInputLayout.removeError();
    }

    public void clearData()
    {
        editText.setText("");
        removeError();
    }
}
