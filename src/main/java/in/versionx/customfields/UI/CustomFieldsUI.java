package in.versionx.customfields.UI;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.versionx.customfields.Interface.FieldsAsyncTaskListner;
import in.versionx.customfields.Database.CustomFieldAsync;
import in.versionx.customfields.Database.FieldDepAsync;
import in.versionx.customfields.Database.FieldOptionsAsync;
import in.versionx.customfields.Utils.GlobalVal;
import in.versionx.customfields.Interface.OnItemSelectListner;
import in.versionx.customfields.Interface.OnScreenChange;
import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptDep;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.Model.Images;
import in.versionx.customfields.R;
import in.versionx.customfields.UI.custom.AutoCompleteTextViewUI_custom;
import in.versionx.customfields.UI.custom.EditTextUI_custom;
import in.versionx.customfields.UI.custom.ImageViewUI_custom;
import in.versionx.customfields.UI.material.AutoCompleteTextViewUI;
import in.versionx.customfields.UI.material.BoxUI;
import in.versionx.customfields.UI.material.CheckBoxUI;
import in.versionx.customfields.UI.material.DatePickerUI;
import in.versionx.customfields.UI.material.EditTextUI;
import in.versionx.customfields.UI.material.ImageViewUI;
import in.versionx.customfields.UI.material.RadioButtonUI;
import in.versionx.customfields.UI.material.SeperationUI;


public class CustomFieldsUI extends LinearLayout implements FieldsAsyncTaskListner, OnItemSelectListner {


    LinearLayout ll_fields;

    String module, hintColor, textColor, underline, textSize, formId = "0";

    public ArrayList<Fields> fieldsList;
    static Context context;

    FieldsLoaded fieldsLoaded;
    AttributeSet attributeSet;

    boolean isCustom;
    private boolean isFirst;
    int selectedScreen = 1;
    int screenCount = 1;
    String screenTitle = "";
    OnScreenChange onScreenChange;
    private SharedPreferences sp;
    public boolean isFront;


    public CustomFieldsUI(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.ll_for_custom_fields, this);


        this.context = context;
        initUI(attributeSet);
        getDbCustomFields(context, module, formId);
        checkPermission();

    }

    public void setFront(boolean front) {

        context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE).edit().putBoolean(GlobalVal.FRONT_CAM, front).apply();
        isFront = front;
    }

    public CustomFieldsUI(Context context) {
        super(context);


    }

    public int getScreenCount() {
        return screenCount;
    }

    public int getSelectedScreen() {
        return selectedScreen;
    }

    public void setSelectedScreen(int selectedScreen) {
        this.selectedScreen = selectedScreen;

        setUI(fieldsList, this.selectedScreen, true);
    }

    public void onBackScreenClicked() {

        selectedScreen--;

        setUI(fieldsList, this.selectedScreen, false);


    }

    public String getScreenTitle(int selectedScreen) {

        return sp.getString("screen" + selectedScreen, "");
    }

    public void setScreenTitle(String screenTitle, int selectedScreen) {
        sp.edit().putString("screen" + selectedScreen, screenTitle).apply();
        this.screenTitle = screenTitle;
    }

    private void initUI(AttributeSet attributeSet) {
        ll_fields = findViewById(R.id.ll_fields);
        fieldsList = new ArrayList<>();
        this.attributeSet = attributeSet;
        sp = context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE);
        TypedArray ta = context.obtainStyledAttributes(attributeSet, R.styleable.CustomFieldsUI, 0, 0);
        this.module = ta.getString(R.styleable.CustomFieldsUI_module);
        this.hintColor = ta.getString(R.styleable.CustomFieldsUI_hintColor);
        this.textColor = ta.getString(R.styleable.CustomFieldsUI_textColor);
        this.isCustom = ta.getBoolean(R.styleable.CustomFieldsUI_custom, false);
        this.underline = ta.getString(R.styleable.CustomFieldsUI_underlineColor);
        this.textSize = ta.getString(R.styleable.CustomFieldsUI_textSize);
        this.formId = ta.getString(R.styleable.CustomFieldsUI_formId);
    }


    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && context.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
            } else {

                while (!(context instanceof Activity) && context instanceof ContextWrapper) {
                    context = ((ContextWrapper) context).getBaseContext();
                }


                Activity activity = (Activity) context;
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, 401);
            }
        } else {
            // if version is below m then write code here,
        }
    }

    public void setListner(FieldsLoaded fieldsLoaded) {
        this.fieldsLoaded = fieldsLoaded;
    }

    public void setOnScreenChangeListner(OnScreenChange onScreenChange) {
        this.onScreenChange = onScreenChange;
    }

    public void OnNextPressed() {
        screenCount = sp.getInt(GlobalVal.SCREEN_COUNT, 1);


        //if (validateByScreen()) {
        selectedScreen++;
        setUI(fieldsList, selectedScreen, false);
        // }

    }


    private void setUI(List<Fields> fields, int selectedScreen, boolean isClear) {


        screenCount = context.getSharedPreferences(GlobalVal.CUSTOMFIELD_SP, Context.MODE_PRIVATE).getInt(GlobalVal.SCREEN_COUNT, 1);

        if (onScreenChange != null)
            onScreenChange.setOnScreenChange(selectedScreen, screenCount);

        for (int i = 0; i < fields.size(); i++) {
            //   if (!fields.get(i).getId().equals("sep")) {
            final Fields fieldInfo = fields.get(i);
            if (fieldInfo.getView() == null) {
                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.TEXTBOX)
                        || fieldInfo.getType().equalsIgnoreCase(GlobalVal.NUMBER)
                        || fieldInfo.getType().equalsIgnoreCase(GlobalVal.DECIMAL_NUMBER)
                        || fieldInfo.getType().equals(GlobalVal.TEXTAREA)) {


                    if (isCustom) {

                        EditTextUI_custom editTextUI_custom = new EditTextUI_custom(context, attributeSet);
                        editTextUI_custom.setTextColor(textColor);
                        editTextUI_custom.setHintColor(hintColor);
                        editTextUI_custom.setTextSize(textSize);
                        editTextUI_custom.setField(fieldInfo);
                        fieldsList.add(editTextUI_custom.getField());
                        ll_fields.addView(editTextUI_custom);

                    } else {

                        EditTextUI ediTextUI = new EditTextUI(context, attributeSet);
                        ediTextUI.setTextColor(textColor);
                        ediTextUI.setHintColor(hintColor);
                        ediTextUI.setTextSize(textSize);
                        ediTextUI.setField(fieldInfo);
                        ediTextUI.setUnderlineColor(underline);
                        fieldsList.add(ediTextUI.getField());
                        ll_fields.addView(ediTextUI);
                    }

                }


                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.DROPDOWN)) {

                    if (isCustom) {
                        AutoCompleteTextViewUI_custom autoCompleteTextViewUI = new AutoCompleteTextViewUI_custom(context, attributeSet);
                        autoCompleteTextViewUI.setTextColor(textColor);
                        autoCompleteTextViewUI.setHintColor(hintColor);
                        autoCompleteTextViewUI.setTextSize(textSize);
                        autoCompleteTextViewUI.setField(fieldInfo);
                        fieldsList.add(autoCompleteTextViewUI.getField());
                        ll_fields.addView(autoCompleteTextViewUI);

                    } else {
                        AutoCompleteTextViewUI autoCompleteTextViewUI = new AutoCompleteTextViewUI(context, attributeSet);
                        autoCompleteTextViewUI.setTextColor(textColor);
                        autoCompleteTextViewUI.setHintColor(hintColor);
                        autoCompleteTextViewUI.setTextSize(textSize);
                        autoCompleteTextViewUI.setField(fieldInfo);
                        fieldsList.add(autoCompleteTextViewUI.getField());
                        ll_fields.addView(autoCompleteTextViewUI);
                    }


                }


                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.RADIOBUTTON)) {

                    RadioButtonUI radioButtonUI = new RadioButtonUI(context, attributeSet);
                    radioButtonUI.setTextColor(textColor);
                    radioButtonUI.setHintColor(hintColor);
                    radioButtonUI.setTextSize(textSize);
                    radioButtonUI.setField(fieldInfo);
                    fieldsList.add(radioButtonUI.getField());
                    ll_fields.addView(radioButtonUI);


                }


                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.CHECKBOX)) {


                    CheckBoxUI checkBoxUI = new CheckBoxUI(context, attributeSet);
                    checkBoxUI.setTextColor(textColor);
                    checkBoxUI.setHintColor(hintColor);
                    checkBoxUI.setTextSize(textSize);
                    checkBoxUI.setField(fieldInfo);
                    fieldsList.add(checkBoxUI.getField());
                    ll_fields.addView(checkBoxUI);


                }


                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.SEPERATION)) {

                    SeperationUI seperationUI = new SeperationUI(context, attributeSet);
                    seperationUI.setTextColor(textColor);
                    seperationUI.setHintColor(hintColor);
                    fieldsList.add(seperationUI.getView(fieldInfo));
                    ll_fields.addView(seperationUI);


                }


                if (fieldInfo.getType().equalsIgnoreCase(GlobalVal.IMG)) {


                    if (!isCustom) {

                        ImageViewUI imageViewUI = new ImageViewUI(context, attributeSet);
                        imageViewUI.setTextColor(textColor);
                        imageViewUI.setHintColor(hintColor);
                        imageViewUI.setModule(module);
                        imageViewUI.setFront(isFront);
                        imageViewUI.setMulti(fieldInfo.isMulti);
                        imageViewUI.setAlignment(fieldInfo.align);
                        imageViewUI.setField(fieldInfo);
                        fieldsList.add(imageViewUI.getField());
                        ll_fields.addView(imageViewUI);
                    } else {

                        ImageViewUI_custom imageViewUI_custom = new ImageViewUI_custom(context, attributeSet);
                        imageViewUI_custom.setTextColor(textColor);
                        imageViewUI_custom.setHintColor(hintColor);
                        imageViewUI_custom.setFront(isFront);
                        imageViewUI_custom.setMulti(fieldInfo.isMulti);
                        imageViewUI_custom.setAlignment(fieldInfo.align);
                        imageViewUI_custom.setField(fieldInfo);
                        fieldsList.add(imageViewUI_custom.getField());
                        ll_fields.addView(imageViewUI_custom);
                    }


                }


                if (fieldInfo.getType().equals(GlobalVal.BOXES)) {

                    BoxUI boxUI = new BoxUI(context, attributeSet);
                    boxUI.setTextColor(textColor);
                    boxUI.setHintColor(hintColor);
                    boxUI.setTextSize(textSize);
                    boxUI.setField(fieldInfo);
                    fieldsList.add(boxUI.getField());
                    ll_fields.addView(boxUI);

                }

                if (fieldInfo.getType().equals(GlobalVal.DATE)) {
                    DatePickerUI datePickerUI = new DatePickerUI(context, attributeSet);
                    datePickerUI.setTextColor(textColor);
                    datePickerUI.setHintColor(hintColor);
                    datePickerUI.setTextSize(textSize);
                    datePickerUI.setField(fieldInfo);
                    fieldsList.add(datePickerUI.getField());
                    ll_fields.addView(datePickerUI);
                }


            }


            if (fieldInfo.getView() != null) {

                if (fieldInfo.getScreen() != selectedScreen || fieldInfo.isHide() || (isClear && fieldInfo.isDep()
                        || (fieldInfo.isDep() && fieldInfo.getText().trim().isEmpty() && !isClear)))
                    fieldInfo.getView().setVisibility(View.GONE);
                else
                    fieldInfo.getView().setVisibility(View.VISIBLE);
            }


        }


    }


    public void getDbCustomFields(Context context, String mod, String formId) {
        CustomFieldAsync customFieldAsync = new CustomFieldAsync(context, "GET", mod, formId);
        customFieldAsync.setListner(this);
        customFieldAsync.execute();
    }


    @Override
    public void OnFieldsLoaded(List<Fields> customFields) {
        isFirst = true;
        setUI(customFields, selectedScreen, true);


        FieldOptionsAsync fieldOptionsAsync = new FieldOptionsAsync(context, "GETALL", null, null);
        fieldOptionsAsync.setListner(this);
        fieldOptionsAsync.execute();
        fieldsLoaded.onFieldsLoaded();
        fieldsLoaded.onFieldsLoaded(customFields);


    }

    @Override
    public void OnOptionsLoaded(List<FieldsOptions> customFields) {

        setFieldsOptions(customFields, null, null);


    }


    @Override
    public void onDepOptLoaded(List<FieldsOptDep> depOptions, ArrayList<String> depFieldsId, Fields fields) {


        if (depFieldsId == null) {
            depOptions = new ArrayList<>();
        }
        if (depOptions != null)
            updateDependency(depOptions, depFieldsId, fields);
    }

    @Override
    public void onDepClear(ArrayList<String> depFieldIds) {

        for (int i = 0; i < fieldsList.size(); i++) {
            if (fieldsList.get(i).isDep()) {

                switch (fieldsList.get(i).getType()) {

                    case GlobalVal.TEXTAREA:
                    case GlobalVal.TEXTBOX:
                        if (isCustom) {
                            EditTextUI_custom editTextUI = (EditTextUI_custom) fieldsList.get(i).uiInstance;
                            editTextUI.clearDep(depFieldIds);
                        } else {

                            EditTextUI editTextUI = (EditTextUI) fieldsList.get(i).uiInstance;
                            editTextUI.clearDep(depFieldIds);
                        }

                        break;
                    case GlobalVal.DATE:

                        DatePickerUI datePickerUI = (DatePickerUI) fieldsList.get(i).uiInstance;
                        datePickerUI.clearDep(depFieldIds);

                        break;

                    case GlobalVal.RADIOBUTTON:

                        RadioButtonUI radioButtonUI = (RadioButtonUI) fieldsList.get(i).uiInstance;
                        radioButtonUI.clearDep(depFieldIds);

                        break;
                    case GlobalVal.DROPDOWN:

                        if (isCustom) {
                            AutoCompleteTextViewUI_custom autoCompleteTextViewUI = (AutoCompleteTextViewUI_custom) fieldsList.get(i).uiInstance;
                            autoCompleteTextViewUI.clearDep(depFieldIds);
                        } else {
                            AutoCompleteTextViewUI autoCompleteTextViewUI = (AutoCompleteTextViewUI) fieldsList.get(i).uiInstance;
                            autoCompleteTextViewUI.clearDep(depFieldIds);
                        }


                        break;
                    case GlobalVal.BOXES:

                        BoxUI boxUI = (BoxUI) fieldsList.get(i).uiInstance;
                        boxUI.clearDep(depFieldIds);


                        break;

                    case GlobalVal.CHECKBOX:
                        CheckBoxUI checkBoxUI = (CheckBoxUI) fieldsList.get(i).uiInstance;
                        checkBoxUI.clearDep(depFieldIds);
                        break;


                }


            }
        }

    }


    private void updateDependency(List<FieldsOptDep> fieldsOptDeps, ArrayList<String> depFieldsId, Fields fields) {


        ArrayList<FieldsOptions> options = new ArrayList<>();
        for (FieldsOptDep fieldsOptDep : fieldsOptDeps) {
            FieldsOptions fieldsOptions = new FieldsOptions();
            fieldsOptions.setfId(fieldsOptDep.getfId());
            fieldsOptions.setpId(fieldsOptDep.getpId());
            fieldsOptions.setOptionId(fieldsOptDep.getOptId());
            fieldsOptions.setName(fieldsOptDep.getOptNm());
            options.add(fieldsOptions);
        }


        setDepOptions(options, depFieldsId, fields);
    }


    private void setFieldsOptions(final List<FieldsOptions> options, ArrayList<String> depFieldsId, Fields fields) {


        for (int i = 0; i < fieldsList.size(); i++) {
            if (fieldsList.get(i).isDep() || fields == null) {

                switch (fieldsList.get(i).getType()) {

                    case GlobalVal.RADIOBUTTON:
                        if (options != null && options.size() > 0) {
                            RadioButtonUI radioButtonUI = (RadioButtonUI) fieldsList.get(i).uiInstance;
                            radioButtonUI.setFieldsOptions(options);
                            radioButtonUI.setListner(this);
                        }
                        break;
                    case GlobalVal.DROPDOWN:
                        if (options != null && options.size() > 0) {
                            if (isCustom) {
                                AutoCompleteTextViewUI_custom autoCompleteTextViewUI = (AutoCompleteTextViewUI_custom) fieldsList.get(i).uiInstance;
                                autoCompleteTextViewUI.setFieldsOptions(options);
                                autoCompleteTextViewUI.setListner(this);
                            } else {
                                AutoCompleteTextViewUI autoCompleteTextViewUI = (AutoCompleteTextViewUI) fieldsList.get(i).uiInstance;
                                autoCompleteTextViewUI.setFieldsOptions(options);
                                autoCompleteTextViewUI.setListner(this);
                            }

                        }

                        break;
                    case GlobalVal.BOXES:
                        if (options != null && options.size() > 0) {
                            BoxUI boxUI = (BoxUI) fieldsList.get(i).uiInstance;
                            boxUI.setFieldsOptions(options);
                            boxUI.setListner(this);
                        }


                        break;

                    case GlobalVal.CHECKBOX:
                        CheckBoxUI checkBoxUI = (CheckBoxUI) fieldsList.get(i).uiInstance;
                        checkBoxUI.setFieldsOptions(options);
                        break;


                }

                if (fieldsList.get(i).getView() != null) {

                    if (fieldsList.get(i).getScreen() != selectedScreen || fieldsList.get(i).isHide()
                            || fieldsList.get(i).isDep())
                        fieldsList.get(i).getView().setVisibility(View.GONE);
                    else
                        fieldsList.get(i).getView().setVisibility(View.VISIBLE);
                }

            }


        }
    }


    private void setDepOptions(final List<FieldsOptions> options, ArrayList<String> depFieldsId, Fields fields) {


        for (int i = 0; i < fieldsList.size(); i++) {
            if (fieldsList.get(i).isDep()) {

                switch (fieldsList.get(i).getType()) {

                    case GlobalVal.TEXTAREA:
                    case GlobalVal.TEXTBOX:
                        if (isCustom) {
                            EditTextUI_custom editTextUI = (EditTextUI_custom) fieldsList.get(i).uiInstance;
                            editTextUI.setDep(depFieldsId, options);
                        } else {

                            EditTextUI editTextUI = (EditTextUI) fieldsList.get(i).uiInstance;
                            editTextUI.setDep(depFieldsId, options);
                        }

                        break;
                    case GlobalVal.DATE:

                        DatePickerUI datePickerUI = (DatePickerUI) fieldsList.get(i).uiInstance;
                        datePickerUI.setDep(depFieldsId, options);

                        break;

                    case GlobalVal.RADIOBUTTON:
                        if (options != null && options.size() > 0) {
                            RadioButtonUI radioButtonUI = (RadioButtonUI) fieldsList.get(i).uiInstance;
                            radioButtonUI.setDep(depFieldsId, options);
                            radioButtonUI.setFieldsOptions(options);
                        }
                        break;
                    case GlobalVal.DROPDOWN:

                        if (isCustom) {
                            AutoCompleteTextViewUI_custom autoCompleteTextViewUI = (AutoCompleteTextViewUI_custom) fieldsList.get(i).uiInstance;
                            autoCompleteTextViewUI.setDep(depFieldsId, options);
                            autoCompleteTextViewUI.setFieldsOptions(options);
                        } else {
                            AutoCompleteTextViewUI autoCompleteTextViewUI = (AutoCompleteTextViewUI) fieldsList.get(i).uiInstance;
                            autoCompleteTextViewUI.setDep(depFieldsId, options);
                            autoCompleteTextViewUI.setFieldsOptions(options);
                        }


                        break;
                    case GlobalVal.BOXES:
                        if (options != null && options.size() > 0) {
                            BoxUI boxUI = (BoxUI) fieldsList.get(i).uiInstance;
                            boxUI.setDep(depFieldsId, options);
                            boxUI.setFieldsOptions(options);
                            boxUI.setListner(this);
                        }


                        break;

                    case GlobalVal.CHECKBOX:
                        CheckBoxUI checkBoxUI = (CheckBoxUI) fieldsList.get(i).uiInstance;
                        checkBoxUI.setDep(depFieldsId, options);
                        checkBoxUI.setFieldsOptions(options);
                        break;


                }


            }
        }
    }


    public boolean validateDatafields() {

        boolean valid = true;


        for (Fields fields : fieldsList) {

            if (fields.isRequired() && !fields.isHide()) {

                switch (fields.getType()) {
                    case GlobalVal.TEXTBOX:
                    case GlobalVal.TEXTAREA:
                    case GlobalVal.NUMBER:
                    case GlobalVal.DECIMAL_NUMBER:

                        if (isCustom) {
                            EditTextUI_custom editTextUI_custom = (EditTextUI_custom) (fields.uiInstance);
                            EditText editText = (EditText) (fields.getFieldView());
                            if (editText.getText().toString().trim().isEmpty()) {
                                editTextUI_custom.setError("Can't be blank");
                                valid = false;
                            } else {
                                editTextUI_custom.removeError();
                                if (valid != false)
                                    valid = true;
                            }

                        } else {

                            EditText editText = (EditText) (fields.getFieldView());
                            if (editText.getText().toString().trim().isEmpty()) {
                                editText.setError("Can't be blank");
                                valid = false;
                            } else {
                                editText.setError(null);
                                if (valid != false)
                                    valid = true;
                            }
                        }

                        break;
                    case GlobalVal.DROPDOWN:

                        AutoCompleteTextView dropDown = (AutoCompleteTextView) (fields.getFieldView());

                        if (dropDown.getText().toString().trim().isEmpty()) {
                            dropDown.setError("Can't be blank");
                            valid = false;
                        } else if (!isValidateOptions(fields, dropDown.getText().toString().trim())) {
                            dropDown.setError("Select from list");
                            valid = false;
                        } else {
                            dropDown.setError(null);
                            if (valid != false)
                                valid = true;
                        }
                        break;


                    case GlobalVal.RADIOBUTTON:

                        RadioGroup radioGroup = (RadioGroup) (fields.getFieldView());
                        RadioButtonUI radioButtonUI = (RadioButtonUI) fields.uiInstance;

                        if (radioGroup.getCheckedRadioButtonId() == -1) {
                            radioButtonUI.setError();
                            valid = false;
                        } else {
                            radioButtonUI.removeError();
                            if (valid != false)
                                valid = true;
                        }
                        break;

                    case GlobalVal.CHECKBOX:

                        boolean isSelectedCb = false;

                        CheckBoxUI checkBoxUI = (CheckBoxUI) fields.uiInstance;

                        ArrayList<CheckBox> checkBoxs= checkBoxUI.getCheckBoxes();

                        for(CheckBox checkBox :checkBoxs)
                        {
                            if(checkBox.isChecked())
                            {
                                isSelectedCb = true;
                                break;
                            }
                        }

                        if (isSelectedCb) {
                            checkBoxUI.removeError();
                            if (valid != false)
                                valid = true;
                        } else {
                            checkBoxUI.setError();
                            valid = false;
                        }

                        break;

                    case GlobalVal.BOXES:
                        boolean isSelected = false;

                        for (FieldsOptions options : fields.getOptions()) {

                            if (options.getName().equals(fields.getText())) {
                                isSelected = true;
                                break;
                            }


                        }


                        if (isSelected) {
                            View view = fields.getView();

                            TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                            //  box_heading.setTextColor(context.getResources().getColor(R.color.app_text_color));

                            // box_heading.setTextColor(Color.LTGRAY);
                            box_heading.setTextColor(Color.parseColor(textColor));


                            if (valid != false)
                                valid = true;
                        } else {
                            View view = fields.getView();

                            TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                            box_heading.setTextColor(Color.RED);
                            valid = false;
                        }

                        break;


                    case GlobalVal.IMG:

                        if (isCustom) {


                            ImageViewUI_custom imageViewUI = (ImageViewUI_custom) (fields.getUiInstance());

                            ArrayList<Images> images = imageViewUI.getImages();


                            if ((images == null || (images != null && images.size() == 0))) {

                                if (fields.getId().equals("img") && module.equals("visitor")) {

                                    imageViewUI.getFieldView().setImageResource(0);
                                    imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                } else {
                                    imageViewUI.getImgtext().setTextColor(Color.RED);
                                }

                                valid = false;

                            } else {
                                imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                if (valid != false)
                                    valid = true;
                            }


                        } else {
                            ImageViewUI imageViewUI = (ImageViewUI) (fields.getUiInstance());

                            Images images = imageViewUI.getImages();


                            if ((images == null || images.getFilePath() == null || images.getFilePath().isEmpty())
                            ) {

                                if (fields.getId().equals("img") && module.equals("visitor")) {

                                    imageViewUI.getFieldView().setImageResource(0);
                                    imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                } else {
                                    imageViewUI.getImgtext().setTextColor(Color.RED);
                                }

                                valid = false;

                            } else {
                                imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                if (valid != false)
                                    valid = true;
                            }

                        }

                        break;


                }
            }
        }

        return valid;
    }

  /*  public boolean validateModuleList(ArrayList<Object> objects, String typ) {

       switch (typ)
       {
           case GlobalVal.STAFF
           {

           }
       }


    }*/


    public boolean valdiateWithoutImgField() {

        boolean valid = true;


        for (Fields fields : fieldsList) {

            if (fields.isRequired() && !fields.isHide() && fields.getScreen() == selectedScreen) {

                switch (fields.getType()) {
                    case GlobalVal.TEXTBOX:
                    case GlobalVal.TEXTAREA:
                    case GlobalVal.NUMBER:
                    case GlobalVal.DECIMAL_NUMBER:

                        EditText editText = (EditText) (fields.getFieldView());

                        if (editText.getText().toString().trim().isEmpty() && editText.isShown()) {

                            editText.setError("Can't be blank");

                            valid = false;

                        } else {
                            editText.setError(null);
                            if (valid != false)
                                valid = true;
                        }

                        break;

                    case GlobalVal.DROPDOWN:

                        AutoCompleteTextView dropDown = (AutoCompleteTextView) (fields.getFieldView());

                        if (dropDown.getText().toString().trim().isEmpty() && dropDown.isShown()) {
                            dropDown.setError("Can't be blank");
                            valid = false;
                        } else if (!isValidateOptions(fields, dropDown.getText().toString().trim())
                                && dropDown.isShown()) {
                            dropDown.setError("Select from list");
                            valid = false;
                        } else {
                            dropDown.setError(null);
                            if (valid != false)
                                valid = true;
                        }
                        break;

                    case GlobalVal.RADIOBUTTON:

                        RadioGroup radioGroup = (RadioGroup) (fields.getFieldView());

                        if (radioGroup.getCheckedRadioButtonId() == -1 && radioGroup.isShown()) {
                            valid = false;
                        } else {
                            if (valid != false)
                                valid = true;
                        }
                        break;

                    case GlobalVal.BOXES:


                        boolean isSelected = false;

                        for (FieldsOptions options : fields.getOptions()) {

                            if (fields.getText().equals(options.getName())) {

                                isSelected = true;
                                break;
                            }


                        }


                        if (isSelected) {
                            View view = fields.getView();

                            TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                            //  box_heading.setTextColor(context.getResources().getColor(R.color.app_text_color));

                            // box_heading.setTextColor(Color.LTGRAY);
                            box_heading.setTextColor(Color.parseColor(textColor));


                            if (valid != false)
                                valid = true;
                        } else {
                            View view = fields.getView();

                            TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                            box_heading.setTextColor(Color.RED);
                            valid = false;
                        }

                        break;

                    case GlobalVal.IMG:

                        if (!fields.getId().equals("img")) {

                            if (isCustom) {


                                ImageViewUI_custom imageViewUI = (ImageViewUI_custom) (fields.getUiInstance());

                                ArrayList<Images> images = imageViewUI.getImages();


                                if ((images == null || (images != null && images.size() == 0)) && imageViewUI.isShown()) {

                                    if (fields.getId().equals("img") && module.equals("visitor")) {

                                        imageViewUI.getFieldView().setImageResource(0);
                                        imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                    } else {
                                        imageViewUI.getImgtext().setTextColor(Color.RED);
                                        imageViewUI.setError("");
                                    }

                                    valid = false;

                                } else {
                                    imageViewUI.setError(null);
                                    imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                    if (valid != false)
                                        valid = true;
                                }


                            } else {
                                ImageViewUI imageViewUI = (ImageViewUI) (fields.getUiInstance());

                                Images images = imageViewUI.getImages();


                                if ((images == null || images.getFilePath() == null || images.getFilePath().isEmpty())
                                        && imageViewUI.isShown()) {

                                    if (fields.getId().equals("img") && module.equals("visitor")) {

                                        imageViewUI.getFieldView().setImageResource(0);
                                        imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                    } else {
                                        imageViewUI.setError("");
                                        //  imageViewUI.getImgtext().setTextColor(Color.RED);
                                    }

                                    valid = false;

                                } else {
                                    imageViewUI.setError(null);
                                    //  imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                    if (valid != false)
                                        valid = true;
                                }

                            }
                        }

                        break;


                }
            }
        }

        return valid;
    }

    public HashMap getData() {

        HashMap<String, Object> hashMap = new HashMap<>();
        // if (valdiateDatafields()) {

        HashMap<String, Object> valMap = null;
        for (Fields fields : fieldsList) {

            switch (fields.getType()) {
                case GlobalVal.NUMBER:
                case GlobalVal.TEXTBOX:
                case GlobalVal.TEXTAREA:
                case GlobalVal.DECIMAL_NUMBER:

                    EditTextUI editTextUI = (EditTextUI) fields.uiInstance;
                    EditText editText = (EditText) editTextUI.getField().getFieldView();

                    if (!editText.getText().toString().trim().isEmpty()) {
                        valMap = new HashMap<>();
                        if (fields.getType().equals(GlobalVal.NUMBER) && !fields.getId().equals("mob")) {
                            valMap.put("val", Long.parseLong(editText.getText().toString()));

                        }
                        if (fields.getType().equals(GlobalVal.DECIMAL_NUMBER) && !fields.getId().equals("mob")) {
                            valMap.put("val", Double.parseDouble(editText.getText().toString()));

                        } else {
                            valMap.put("val", editText.getText().toString());
                        }

                        hashMap.put(fields.getId(), valMap);

                    }

                    break;
                case GlobalVal.RADIOBUTTON:

                    RadioGroup radioGroup = (RadioGroup) fields.getFieldView();
                    HashMap<String, Object> radioMap = new HashMap<>();
                    HashMap<String, Object> radioValMap = new HashMap<>();

                    if (radioGroup.getCheckedRadioButtonId() != -1) {
                        RadioButton radioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());

                        for (FieldsOptions options : fields.getOptions()) {
                            if (options.getName().equalsIgnoreCase(radioButton.getText().toString())) {
                                valMap = new HashMap<>();
                                valMap.put("val", options.getName());

                                radioMap.put(options.getOptionId(), valMap);

                            }
                        }
                        if (valMap.size() > 0) {

                            radioValMap.put("val", radioMap);
                            hashMap.put(fields.getId(), radioValMap);


                        }
                    }


                    break;

                case GlobalVal.DROPDOWN:
                    AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) fields.getFieldView();


                    HashMap<String, Object> autoCompleteMap = new HashMap<>();
                    HashMap<String, Object> autoCompleteValMap = new HashMap<>();
                    valMap = new HashMap<>();
                    if (!autoCompleteTextView.getText().toString().isEmpty()) {
                        if (fields.getOptions() != null) {
                            for (FieldsOptions options : fields.getOptions()) {
                                if (options.getName().equalsIgnoreCase(autoCompleteTextView.getText().toString())) {

                                    valMap.put("val", options.getName());

                                    autoCompleteMap.put(options.getOptionId(), valMap);

                                    break;

                                }
                            }
                        }


                        if (valMap.size() > 0) {

                            autoCompleteValMap.put("val", autoCompleteMap);
                            hashMap.put(fields.getId(), autoCompleteValMap);

                        } else if (!autoCompleteTextView.getText().toString().trim().isEmpty()) {

                            valMap.put("val", autoCompleteTextView.getText().toString());
                            autoCompleteMap.put("0", valMap);
                            autoCompleteValMap.put("val", autoCompleteMap);
                            hashMap.put(fields.getId(), autoCompleteValMap);

                        }
                    }


                    break;

                case GlobalVal.BOXES:

                    BoxUI boxUI = (BoxUI) fields.uiInstance;

                    HashMap<String, Object> boxMap = new HashMap<>();

                    HashMap<String, Object> boxValMap = new HashMap<>();

                    if (boxUI.getField().getOptions() != null) {
                        for (FieldsOptions options : boxUI.getField().getOptions()) {
                            if (options.isSelected) {
                                valMap = new HashMap<>();
                                valMap.put("val", options.getName());

                                boxMap.put(options.getOptionId(), valMap);
                                break;

                            }
                        }
                    }
                    if (valMap.size() > 0) {
                        boxValMap.put("val", boxMap);
                        hashMap.put(fields.getId(), boxValMap);

                    }


                    break;

                case GlobalVal.CHECKBOX:

                    LinearLayout ll_cb = (LinearLayout) fields.getFieldView();

                    HashMap<String, Object> cbMap = new HashMap<>();

                    HashMap<String, Object> cbValMap = new HashMap<>();

                    int i = 0;
                    for (FieldsOptions options : fields.getOptions()) {

                        CheckBox cb = (CheckBox) ll_cb.findViewById(i);
                        if (cb.isChecked()) {
                            valMap = new HashMap<>();
                            valMap.put("val", options.getName());

                            cbMap.put(options.getOptionId(), valMap);

                        }
                        i++;
                    }
                    if (valMap.size() > 0) {
                        cbValMap.put("val", cbMap);
                        hashMap.put(fields.getId(), cbValMap);

                    }


                    break;

                case GlobalVal.DATE:

                    DatePickerUI datePickerUI = (DatePickerUI) fields.getUiInstance();
                    EditText dteditText = (EditText) datePickerUI.getField().getFieldView();


                    if (!datePickerUI.getText().toString().trim().isEmpty()) {
                        valMap = new HashMap<>();

                        valMap.put("val", getDataInMili(dteditText.getText().toString()));


                        hashMap.put(fields.getId(), valMap);

                    }

                    break;


                case GlobalVal.IMG:
                    if (isCustom) {
                        ImageViewUI_custom imageViewUI_custom = (ImageViewUI_custom) fields.getUiInstance();

                        ArrayList<Images> images = imageViewUI_custom.getImages();


                        for (Images img : images) {


                            if (img.filePath != null && img.filePath.contains("http")) {

                                HashMap<String, Object> imgMap = new HashMap<>();
                                HashMap<String, Object> imgValMap = new HashMap<>();
                                valMap = new HashMap<>();

                                imgMap.put("val", img.getFilePath());
                                imgValMap.put(img.getDt() + "", imgMap);
                                valMap.put("val", imgValMap);
                                hashMap.put(fields.getId(), valMap);
                            }
                        }
                    } else {
                        ImageViewUI imageViewUI = (ImageViewUI) fields.getUiInstance();

                        Images images2 = imageViewUI.getImages();


                        if (images2 != null && images2.getFilePath() != null && images2.filePath.contains("http")) {

                            HashMap<String, Object> imgMap = new HashMap<>();
                            HashMap<String, Object> imgValMap = new HashMap<>();
                            valMap = new HashMap<>();

                            imgMap.put("val", images2.getFilePath());
                            imgValMap.put(images2.getDt() + "", imgMap);
                            valMap.put("val", imgValMap);
                            hashMap.put(fields.getId(), valMap);
                        }

                    }


                    break;


            }


        }

        //  clearData();


        return hashMap;
    }


    public void clearScreenData() {

        if (onScreenChange != null)
            onScreenChange.setOnScreenChange(selectedScreen, screenCount);


        for (Fields fields : fieldsList) {


            if (fields.getScreen() == getSelectedScreen() || selectedScreen == 1) {
                fields.getView().setVisibility(View.VISIBLE);
                switch (fields.getType()) {
                    case GlobalVal.NUMBER:
                    case GlobalVal.TEXTBOX:
                    case GlobalVal.TEXTAREA:
                    case GlobalVal.DECIMAL_NUMBER:
                        if (isCustom) {
                            EditTextUI_custom editTextUI_custom = (EditTextUI_custom) fields.uiInstance;
                            editTextUI_custom.clearData();

                        } else {
                            EditTextUI editTextUI = (EditTextUI) fields.uiInstance;
                            editTextUI.setText(fields.getVal());
                        }
                        break;
                    case GlobalVal.RADIOBUTTON:

                        RadioButtonUI radioButtonUI = (RadioButtonUI) fields.uiInstance;
                        radioButtonUI.clearData();


                        break;

                    case GlobalVal.DATE:

                        DatePickerUI datePickerUI = (DatePickerUI) fields.uiInstance;
                        datePickerUI.clearData();

                        break;

                    case GlobalVal.DROPDOWN:

                        if (isCustom) {
                            AutoCompleteTextViewUI_custom autoCompleteTextViewUI = (AutoCompleteTextViewUI_custom) fields.uiInstance;
                            autoCompleteTextViewUI.clearData();
                        } else {

                            AutoCompleteTextViewUI autoCompleteTextViewUI = (AutoCompleteTextViewUI) fields.uiInstance;
                            autoCompleteTextViewUI.setText(fields.getVal());
                            autoCompleteTextViewUI.setError(null);
                        }

                        break;

                    case GlobalVal.IMG:


                        if (isCustom) {

                            ImageViewUI_custom imageViewUI_custom = (ImageViewUI_custom) fields.uiInstance;
                            imageViewUI_custom.clearData();

                        } else {

                            ImageViewUI imageViewUI = (ImageViewUI) fields.uiInstance;
                            imageViewUI.clearData();
                        }
                        break;


                    case GlobalVal.BOXES:


                        BoxUI boxUI = (BoxUI) fields.uiInstance;
                        boxUI.clearData();


                        break;

                    case GlobalVal.CHECKBOX:


                        CheckBoxUI checkBoxUI = (CheckBoxUI) fields.uiInstance;
                        checkBoxUI.clearData();


                        break;

                }
            } else {
                fields.getView().setVisibility(View.GONE);
            }

        }

        setUI(fieldsList, selectedScreen, true);


    }


    public Fields getField(String id) {

        for (int i = 0; i < fieldsList.size(); i++) {

            Fields fields = fieldsList.get(i);
            if (fields.getId().equalsIgnoreCase(id)) {
                return fields;
            }
        }

        return null;
    }


    public static InputFilter getCapsTextNumFilter() {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {

                    if (Character.isLowerCase(source.charAt(i))) {
                        char[] v = new char[end - start];
                        TextUtils.getChars(source, start, end, v, 0);
                        String s = new String(v).toUpperCase();

                        if (source instanceof Spanned) {
                            SpannableString sp = new SpannableString(s);
                            TextUtils.copySpansFrom((Spanned) source,
                                    start, end, null, sp, 0);
                            return sp;
                        } else {
                            return s;
                        }
                    }

                    if (!Character.isLetterOrDigit(source.charAt(i))) {
                        return "";
                    }

                }
                return null;
            }
        };

        return filter;
    }


    // onclick on item , load dependency fields
    @Override
    public void onItemClick(FieldsOptions options, Fields fields) {

        FieldDepAsync fieldDepAsync = new FieldDepAsync(context, options, "GETDEPOPT", fields);
        fieldDepAsync.setListner(CustomFieldsUI.this);
        fieldDepAsync.execute();


    }

    @Override
    public void onItemClear(String fId) {

        FieldDepAsync fieldDepAsync = new FieldDepAsync(context, fId, "CLEARFIELD");
        fieldDepAsync.setListner(CustomFieldsUI.this);
        fieldDepAsync.execute();

    }

    public interface FieldsLoaded {
        void onFieldsLoaded();

        void onFieldsLoaded(List<Fields> fields);

    }


    public void setImage(Intent intent) {

        String fId = intent.getStringExtra("id");
        String path = intent.getStringExtra("path");
        boolean show = intent.getBooleanExtra("show", true);


        final Fields field = getField(fId);

        Images images = new Images();
        images.setDt(System.currentTimeMillis() + "");
        images.setFieldId(fId);
        images.setFilePath(path);

        if (isCustom) {
            ArrayList<Images> imgs = new ArrayList<>();
            imgs.add(images);

            ImageViewUI_custom imageViewUI_custom = (ImageViewUI_custom) field.getUiInstance();
            imageViewUI_custom.setImages(imgs, show);

        } else {
            ImageViewUI imageViewUI = (ImageViewUI) field.getUiInstance();
            imageViewUI.setImages(images, show);

        }


    }


    public void setData(HashMap<String, Object> hashMap, boolean checkReenter) {
        HashMap<String, Object> hashMap1 = (HashMap) hashMap.get("f");

        for (Map.Entry<String, Object> entry : hashMap1.entrySet()) {


            for (Fields fields : fieldsList) {


                if (fields.id.equals(entry.getKey()) && !fields.id.equals("mob")
                        && ((checkReenter && !fields.isReenter()) || !checkReenter)) {
                    switch (fields.getType()) {
                        case GlobalVal.DROPDOWN:

                            if (isCustom) {
                                AutoCompleteTextViewUI_custom autoCompleteTextViewUI = (AutoCompleteTextViewUI_custom) fields.getUiInstance();
                                autoCompleteTextViewUI.setText((HashMap) entry.getValue());
                            } else {
                                AutoCompleteTextViewUI autoCompleteTextViewUI = (AutoCompleteTextViewUI) fields.getUiInstance();
                                autoCompleteTextViewUI.setText((HashMap) entry.getValue());
                            }

                            break;
                        case GlobalVal.TEXTAREA:
                        case GlobalVal.TEXTBOX:
                        case GlobalVal.NUMBER:
                        case GlobalVal.DECIMAL_NUMBER:

                            if (isCustom) {
                                EditTextUI_custom editTextUI = (EditTextUI_custom) fields.getUiInstance();
                                editTextUI.setText((HashMap) entry.getValue());
                            } else {
                                EditTextUI editTextUI = (EditTextUI) fields.getUiInstance();
                                editTextUI.setText((HashMap) entry.getValue());
                            }


                            break;


                        case GlobalVal.RADIOBUTTON:
                            RadioButtonUI radioButtonUI = (RadioButtonUI) fields.getUiInstance();
                            radioButtonUI.setSelected((HashMap) entry.getValue());

                            break;

                        case GlobalVal.IMG:

                            if (isCustom) {
                                ImageViewUI_custom imageViewUI = (ImageViewUI_custom) fields.getUiInstance();
                                imageViewUI.setImageVal((HashMap) entry.getValue());
                            } else {
                                ImageViewUI imageViewUI = (ImageViewUI) fields.getUiInstance();
                                imageViewUI.setImageVal((HashMap) entry.getValue());
                            }


                            break;


                        case GlobalVal.BOXES:

                            BoxUI boxUI = (BoxUI) fields.getUiInstance();
                            boxUI.setSelectedBox((HashMap) entry.getValue());


                            break;

                        case GlobalVal.CHECKBOX:
                            CheckBoxUI checkBoxUI = (CheckBoxUI) fields.getUiInstance();
                            checkBoxUI.setSelectedCheckBox((HashMap) entry.getValue());


                            break;

                    }


                }


            }
        }
    }


    public ArrayList<Images> getImages() {

        ArrayList<Images> images = new ArrayList<>();

        for (Fields fields : fieldsList) {
            if (fields.getType().equals(GlobalVal.IMG)) {
                if (isCustom) {
                    ImageViewUI_custom imageViewUI_custom = (ImageViewUI_custom) fields.getUiInstance();
                    ArrayList<Images> imgs = imageViewUI_custom.getImages();


                    for (Images image : imgs) {
                        if (image != null && image.getFilePath() != null) {

                            images.add(image);
                        }
                    }
                } else {
                    ImageViewUI imageViewUI = (ImageViewUI) fields.getUiInstance();
                    Images image = imageViewUI.getImages();
                    if (image != null && image.getFilePath() != null) {

                        images.add(image);

                    }
                }


            }
        }

        return images;
    }

    private boolean isValidateOptions(Fields field, String val) {
        if (field.getModLbl() == null) {
            if (!field.isEdt() && field.getOptions() != null) {
                for (FieldsOptions opt : field.getOptions()) {
                    if (opt.getName().equals(val)) {
                        return true;
                    }
                }
            }

            return false;
        } else {
            return true;
        }


    }

    public boolean validateByScreen() {

        boolean valid = true;

        for (Fields fields : fieldsList) {

            if (fields.getScreen() == selectedScreen || fields.getId().equals("img") && module.equals("visitor")) {

                if (fields.isRequired() && !fields.isHide()) {

                    switch (fields.getType()) {
                        case GlobalVal.TEXTBOX:
                        case GlobalVal.TEXTAREA:
                        case GlobalVal.NUMBER:
                        case GlobalVal.DECIMAL_NUMBER:

                            EditText editText = (EditText) (fields.getFieldView());

                            if (editText.getText().toString().trim().isEmpty() && editText.isShown()) {

                                editText.setError("Can't be blank");

                                valid = false;

                            } else {
                                editText.setError(null);
                                if (valid != false)
                                    valid = true;
                            }

                            break;

                        case GlobalVal.DROPDOWN:

                            AutoCompleteTextView dropDown = (AutoCompleteTextView) (fields.getFieldView());

                            if (dropDown.getText().toString().trim().isEmpty() && dropDown.isShown()) {
                                dropDown.setError("Can't be blank");
                                valid = false;
                            } else if (!isValidateOptions(fields, dropDown.getText().toString().trim()) && dropDown.isShown()) {
                                dropDown.setError("Select from list");
                                valid = false;
                            } else {
                                dropDown.setError(null);
                                if (valid != false)
                                    valid = true;
                            }
                            break;


                        case GlobalVal.RADIOBUTTON:

                            RadioGroup radioGroup = (RadioGroup) (fields.getFieldView());
                            RadioButtonUI radioButtonUI = (RadioButtonUI) fields.uiInstance;

                            if (radioGroup.getCheckedRadioButtonId() == -1) {
                                radioButtonUI.setError();
                                valid = false;
                            } else {
                                radioButtonUI.removeError();
                                if (valid != false)
                                    valid = true;
                            }
                            break;


                        case GlobalVal.BOXES:

                            BoxUI boxUI = (BoxUI) fields.uiInstance;


                            boolean isSelected = false;

                            for (FieldsOptions options : fields.getOptions()) {

                                if (fields.getText().equals(options.getName())) {

                                    isSelected = true;
                                    break;
                                }


                            }


                            if (isSelected) {
                                View view = fields.getView();

                                TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                                //  box_heading.setTextColor(context.getResources().getColor(R.color.app_text_color));

                                // box_heading.setTextColor(Color.LTGRAY);
                                box_heading.setTextColor(Color.parseColor(textColor));


                                if (valid != false)
                                    valid = true;
                            } else {
                                View view = fields.getView();

                                TextView box_heading = (TextView) view.findViewById(R.id.box_heading);
                                box_heading.setTextColor(Color.RED);
                                valid = false;
                            }

                            break;


                        case GlobalVal.IMG:

                            if (isCustom) {


                                ImageViewUI_custom imageViewUI = (ImageViewUI_custom) (fields.getUiInstance());

                                ArrayList<Images> images = imageViewUI.getImages();


                                if ((images == null || (images != null && images.size() == 0))) {

                                    if (fields.getId().equals("img") && module.equals("visitor")) {

                                        imageViewUI.getFieldView().setImageResource(0);
                                        imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                        valid = false;
                                    } else if (imageViewUI.isShown()) {
                                        imageViewUI.getImgtext().setTextColor(Color.RED);
                                        valid = false;
                                    } else {
                                        if (valid != false)
                                            valid = true;
                                    }


                                } else {
                                    imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                    if (valid != false)
                                        valid = true;
                                }


                            } else {
                                ImageViewUI imageViewUI = (ImageViewUI) (fields.getUiInstance());

                                Images images = imageViewUI.getImages();


                                if ((images == null || images.getFilePath() == null || images.getFilePath().isEmpty())) {

                                    if (fields.getId().equals("img") && module.equals("visitor")) {
                                        imageViewUI.getFieldView().setImageResource(0);
                                        imageViewUI.getFieldView().setImageResource(R.drawable.default_user_red);
                                        valid = false;
                                    } else if (imageViewUI.isShown()) {
                                        imageViewUI.getImgtext().setTextColor(Color.RED);
                                        valid = false;
                                    } else {
                                        if (valid != false)
                                            valid = true;
                                    }

                                } else {
                                    imageViewUI.getImgtext().setTextColor(Color.parseColor(textColor));
                                    if (valid != false)
                                        valid = true;
                                }

                            }

                            break;


                    }
                }
            }
        }

        return valid;
    }

    public long getDataInMili(String dt) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        Date date = null;
        try {
            date = sdf.parse(dt);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;

    }


}
