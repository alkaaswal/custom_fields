package in.versionx.customfields.Interface;


import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;

public interface OnItemSelectListner {

    void onItemClick(FieldsOptions options, Fields field);

    void onItemClear(String fId);
}
