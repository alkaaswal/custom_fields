package in.versionx.customfields.Interface;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptDep;
import in.versionx.customfields.Model.FieldsOptions;


public interface FieldsAsyncTaskListner {

    public void OnFieldsLoaded(List<Fields> customFields);

    public void OnOptionsLoaded(List<FieldsOptions> customFields);

    //  public void OnMappingLoaded(List<FieldMapping> mappedOptions, String fId, String flag);


    void onDepOptLoaded(List<FieldsOptDep> depOptions, ArrayList<String> depFieldIds, Fields fields);

    void onDepClear( ArrayList<String> depFieldIds);

    //public void OnAppointMentSearchResult(List<Appointment> appointments, String searchTxt);
}
