package in.versionx.customfields.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.util.ArrayList;

import in.versionx.customfields.Model.Images;
import in.versionx.customfields.R;


/**
 * Created by developer on 2/12/16.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    Context context;
    ArrayList<Images> imgList;
    ArrayList<Integer> pos;

    SharedPreferences loginSp;

    public ImageAdapter(Context context, ArrayList<Images> imgList) {
        this.imgList = imgList;
        this.context = context;
        pos = new ArrayList<>();
    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.multi_img_item, parent, false);

        // Return a new holder instance

        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder viewHolder, int position) {


        try {

            Glide.with(context).load(imgList.get(position).getFilePath()).asBitmap().fitCenter().override(300, 300).fitCenter().
                    diskCacheStrategy(DiskCacheStrategy.RESULT).
                    into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                            viewHolder.iv_img.setImageBitmap(resource);
                        }
                    });

        } catch (Exception e) {
            viewHolder.iv_img.setImageResource(R.drawable.ic_image_custom);
        }




     /*   viewHolder.in_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                zoomImg(imgList.get(position));

            }
        });*/


    }

    /* private void zoomImg(final String url, final String imgKey, final int position, String matKey) {
         final Dialog dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
         dialog.setContentView(R.layout.dialog_img);
         dialog.setCancelable(true);
         *//*ImageView iv_idProof = (ImageView) dialog.findViewById(R.id.iv_dialog_idProof);*//*
        final TouchImageView iv_idProof = (TouchImageView) dialog.findViewById(R.id.iv_dialog_idProof);
        final ImageView deleteImg = (ImageView) dialog.findViewById(R.id.deleteImg);
        if (!url.contains("https:")) {
            try {

                Glide.with(context).load(url).asBitmap().fitCenter().
                        override(500, 600).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                        iv_idProof.setImageBitmap(resource);
                        deleteImg.setVisibility(View.VISIBLE);

                    }
                });


            } catch (Exception e) {
                deleteImg.setVisibility(View.GONE);
                iv_idProof.setImageResource(R.drawable.default_user_picture);
            }
        } else {


            Glide.with(context).load(url).asBitmap().fitCenter().
                    override(500, 600).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

                    iv_idProof.setImageBitmap(resource);
                    deleteImg.setVisibility(isEdit ? View.VISIBLE : View.GONE);

                }
            });



          *//*  deleteImg.setVisibility(View.GONE);
            imageLoaderOriginal.DisplayImage(url, iv_idProof);*//*
        }

        dialog.show();

        deleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showConfirmationDialog(context, url, imgKey,position, dialog);
            }
        });


    }

*/
    private Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_img;

        ImageViewHolder(View itemView) {
            super(itemView);
            iv_img = (ImageView) itemView.findViewById(R.id.iv_img);

            iv_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //  zoomImg(imgList.get(getAdapterPosition()).getVal(),imgList.get(getAdapterPosition()).getKey(),getAdapterPosition(),matKey);

                }
            });
        }
    }

    public static Bitmap rotateBitmap(String sourcepath, Bitmap bitmap, String imageKey, Context context) {
        int rotate = 0;

        try {
            File imageFile = new File(sourcepath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {


        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);


        return bitmap;

    }


    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

}
