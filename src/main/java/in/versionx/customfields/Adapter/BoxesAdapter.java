package in.versionx.customfields.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.Model.Fields;
import in.versionx.customfields.Model.FieldsOptions;
import in.versionx.customfields.R;

public class BoxesAdapter extends RecyclerView.Adapter<BoxesAdapter.MyViewHolder> {


    List<FieldsOptions> options;
    Context context;

    String type;

    BoxItemSelected boxItemSelected;
    Fields field;

    public BoxesAdapter(Context context, List<FieldsOptions> options, BoxItemSelected boxItemSelected, Fields fields) {
        this.options = options;
        this.context = context;
        this.type = type;
        field = fields;
        this.boxItemSelected = boxItemSelected;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View view = inflater.inflate(R.layout.box_item, parent, false);

        // Return a new holder instance
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder viewHolder, final int position) {

        viewHolder.boxField.setText(options.get(position).getName());

        if (options.get(position).isSelected()) {
            viewHolder.boxField.setBackgroundColor(Color.DKGRAY);
            boxItemSelected.onBoxItemSelected(options);

        } else {
            viewHolder.boxField.setBackgroundColor(Color.LTGRAY);

        }

        viewHolder.boxField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < options.size(); i++) {
                    options.get(i).setSelected(false);
                }
                for (int i = 0; i < field.getOptions().size(); i++) {
                    field.getOptions().get(i).setSelected(false);


                    if (options.get(position).optionId.equals(field.getOptions().get(i).getOptionId())
                            && !field.getOptions().get(i).isSelected()) {
                        field.getOptions().get(i).setSelected(true);
                        options.get(position).setSelected(true);

                        //   System.out.println("alka selected inside " + field.getOptions().get(i).isSelected());


                        break;
                    }

                }

                notifyDataSetChanged();


            }
        });


    }

    private Context getContext() {
        return context;
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        Button boxField;

        public MyViewHolder(View itemView) {
            super(itemView);


            boxField = itemView.findViewById(R.id.field_box);


        }
    }

       /* public void filter(String charText, ArrayList<Visitor> visitorList) {
            try {
                charText = charText.toLowerCase(Locale.getDefault());
                Iterator<Visitor> it = visitorList.iterator();
                int count = 0;
                if (filterData.size() == 0) {
                    filterData.addAll(visitorList);
                }
                visitorList.clear();
                if (charText.length() == 0) {
                    visitorList.addAll(filterData);
                } else {
                    for (int i = 0; i < filterData.size(); i++) {

                        Visitor visitor = filterData.get(i);

                        if (visitor.getName().toLowerCase().contains(charText) || visitor.getMobile().contains(charText) || visitor.getToMeetNm().contains(charText)) {
                            visitorList.add(visitor);
                        }
                    }
                }
            } catch (Exception e) {


            }

            notifyDataSetChanged();
        }*/

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface BoxItemSelected {
        void onBoxItemSelected(List<FieldsOptions> fieldsOptions);
    }


}
