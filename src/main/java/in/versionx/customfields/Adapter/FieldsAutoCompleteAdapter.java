package in.versionx.customfields.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.versionx.customfields.Model.FieldsOptions;


/**
 * Created by developer on 6/9/16.
 */
public class FieldsAutoCompleteAdapter extends ArrayAdapter<FieldsOptions> {
    private Context context;
    private int resource;
    private List<FieldsOptions> optionItems;
    private ArrayList<FieldsOptions> tempItems;
    private ArrayList<FieldsOptions> suggestions;

    public FieldsAutoCompleteAdapter(Context context, int resource, List<FieldsOptions> items) {
        super(context, resource, 0, items);

        this.context = context;
        this.resource = resource;
        this.optionItems = items;
         tempItems = new ArrayList<FieldsOptions>(this.optionItems);
        suggestions = new ArrayList<FieldsOptions>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(resource, parent, false);
        }

        FieldsOptions options = optionItems.get(position);

        if (options != null && view instanceof TextView) {
            if (!options.getName().equals(null))
                ((TextView) view).setText(options.getName());


        }

        return view;
    }

  /*  public void setTempItems(List<ToMeet> items) {
        tempItems.clear();
        optionItems.clear();
        optionItems.addAll(items);
        tempItems.addAll(items);
        notifyDataSetChanged();
    }*/


    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue)
        {
            FieldsOptions options = (FieldsOptions) resultValue;

            return options.getName();
        }


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();

                for (FieldsOptions options : tempItems) {
                    if (options.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {

                        suggestions.add(options);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if (results != null && results.count > 0) {
                ArrayList<FieldsOptions> filteredList = (ArrayList<FieldsOptions>) ((ArrayList<FieldsOptions>) results.values).clone();
                clear();
                for (FieldsOptions options : filteredList) {
                    add(options);

                }
                notifyDataSetChanged();
            }
        }
    };
}